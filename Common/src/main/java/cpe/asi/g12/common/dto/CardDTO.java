package cpe.asi.g12.common.dto;

public class CardDTO
{

    //region CardDTO - Attributes

    // CardDTO
    private Integer id;

    // CardBasicDTO
    private String name;

    // CardFullDTO
    private String description;
    private String family;
    private String imageURL;
    //
    private int energy;
    private int health;
    private int attack;
    private int defense;
    //
    private int price;

    //endregion

    /**
     * CardDTO - Empty constructor
     */
    public CardDTO()
    {
        this.id = -1;
        this.name = "";
        this.description = "";
        this.family = "";
        this.imageURL = "";
        this.energy = 0;
        this.health = 0;
        this.attack = 0;
        this.defense = 0;
        this.price = 0;

    }

    //region CardDTO - Getters & Setters

    public Integer getID() { return id; }
    public void setID(Integer id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getFamily() { return family; }
    public void setFamily(String family) { this.family = family; }

    public String getImageURL() { return imageURL; }
    public void setImageURL(String imageURL) { this.imageURL = imageURL; }

    public int getEnergy() { return energy; }
    public void setEnergy(int energy) { this.energy = energy; }

    public int getHealth() { return health; }
    public void setHealth(int health) { this.health = health; }

    public int getAttack() { return attack; }
    public void setAttack(int attack) { this.attack = attack; }

    public int getDefense() { return defense; }
    public void setDefense(int defense) { this.defense = defense; }

    public int getPrice() { return price; }
    public void setPrice(int price) { this.price = price; }

    //endregion

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", family='" + family + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", energy=" + energy +
                ", health=" + health +
                ", attack=" + attack +
                ", defense=" + defense +
                ", price=" + price +
                '}';
    }
}
