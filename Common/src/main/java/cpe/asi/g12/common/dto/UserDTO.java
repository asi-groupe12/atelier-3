package cpe.asi.g12.common.dto;

public class UserDTO
{

    //region UserDTO - Attributes

    // UserDTO
    private Integer id;

    // UserCredentialsDTO
    private String login;
    private String password;

    // UserFullDTO
    private String firstName;
    private String lastName;
    //
    private int balance;

    //endregion

    /**
     * UserDTO - Empty constructor
     */
    public UserDTO()
    {
        this.id = -1;
        this.login = "";
        this.password = "";
        this.firstName = "";
        this.lastName = "";
        this.balance = 0;
    }

    //region UserDTO - Getters & Setters

    public Integer getID() { return id; }
    public void setID(Integer id) { this.id = id; }

    public String getLogin() { return login; }
    public void setLogin(String login) { this.login = login; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public int getBalance() { return balance; }
    public void setBalance(int balance) { this.balance = balance; }

    //endregion

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
