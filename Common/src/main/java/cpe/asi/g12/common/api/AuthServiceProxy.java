package cpe.asi.g12.common.api;

import cpe.asi.g12.common.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "auth", url = "http://localhost:8081")//, url = "${service.auth.address}")
public interface AuthServiceProxy
{
    static final String baseURL = "/auth";

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL + "/login")
    ResponseEntity<Boolean> login(@RequestBody UserDTO userDTO);

    // TODO : Should be PATCH but PATCH is unsupported
    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/credentials")
    ResponseEntity<Boolean> changePassword(@RequestBody UserDTO userDTO);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL + "/account")
    ResponseEntity<UserDTO> register(@RequestBody UserDTO userDOT);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/account")
    ResponseEntity<Boolean> unregister(@RequestBody int id);

    //==================================================================================================================
}