package cpe.asi.g12.common.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;

import java.util.List;

@FeignClient(name = "userdata", url = "http://localhost:8082")//, url = "${service.userdata.address}")
public interface UserDataServiceProxy
{
    static final String baseURL = "/users";

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL)
    List<UserDTO> getUsers();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}")
    ResponseEntity<UserDTO> getUser(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/id/{id}")
    ResponseEntity<UserDTO> getUserByID(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/login/{login}")
    ResponseEntity<UserDTO> getUserByLogin(@PathVariable String login);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL)
    ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO);

    // TODO : Should be PATCH but PATCH is unsupported
    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/{id}")
    ResponseEntity<UpdateResponseDTO<UserDTO>> updateUser(@PathVariable int id, @RequestBody UserDTO userDTO);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{id}")
    ResponseEntity<Boolean> removeUser(@PathVariable int id);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/generate")
    List<UserDTO> generateUsers();

    //==================================================================================================================
}