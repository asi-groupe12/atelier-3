package cpe.asi.g12.common.api;

import cpe.asi.g12.common.dto.GameDTO;
import cpe.asi.g12.common.dto.RoomDTO;
import cpe.asi.g12.common.dto.RoomStateEnum;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "rooms", url = "http://localhost:8085")//, url = "${service.market.address}")
public interface RoomServiceProxy {

    static final String baseURL = "/rooms";

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();
    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL)
    List<RoomDTO> getRooms();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}")
    ResponseEntity<RoomDTO> getRoom(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/id/{id}")
    ResponseEntity<RoomDTO> getRoomByID(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/name/{name}")
    ResponseEntity<RoomDTO> getRoomByName(@PathVariable String name);

    //==================================================================================================================

//    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/{number}")
//    Set<RoomDTO> getRandomRoom(@PathVariable int number);
//
//    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/generate")
//    List<RoomDTO> generateRooms();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL)
    ResponseEntity<RoomDTO> addRoom(@RequestBody RoomDTO room);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{id}")
    ResponseEntity<Boolean> removeRoom(@PathVariable int id);

    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/{id}")
    ResponseEntity<UpdateResponseDTO<RoomDTO>> updateRoom(@PathVariable int id, @RequestBody RoomDTO room);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL + "/{roomID}/player/{playerID}")
    ResponseEntity<RoomDTO> joinRoom(@PathVariable int roomID, @PathVariable int playerID);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{roomID}/player/{playerID}")
    ResponseEntity<Boolean> exitRoom(@PathVariable int roomID, @PathVariable int playerID);

    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/{roomID}/player/{playerID}/card/{cardID}")
    ResponseEntity<Boolean> selectCard(@PathVariable int roomID, @PathVariable int playerID, @PathVariable int cardID);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}/games/current")
    ResponseEntity<GameDTO> getCurrentGame(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}/games/last")
    ResponseEntity<GameDTO> getLastGame(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}/games")
    ResponseEntity<GameDTO> getGames(@PathVariable int id);

    //==================================================================================================================
}
