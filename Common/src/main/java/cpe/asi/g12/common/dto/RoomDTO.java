package cpe.asi.g12.common.dto;

import cpe.asi.g12.common.dto.RoomStateEnum;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RoomDTO
{

    //region RoomDTO - Attributes

    // RoomDTO
    private Integer id;

    private String name;
    private RoomStateEnum state;
    private int betAmount;

    private ConcurrentHashMap<Integer, Integer> players;
    private Set<Integer> games;

    //endregion

    /**
     * RoomDTO - Empty constructor
     */
    public RoomDTO()
    {
        this.id = -1;
        this.name = "";
        this.betAmount = 0;
        this.players = new ConcurrentHashMap<>();
        this.games = new HashSet<>();
    }

    //region RoomDTO - Getters & Setters

    public Integer getID() { return id; }
    public void setID(Integer id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public RoomStateEnum getState() { return state; }
    public void setState(RoomStateEnum state) { this.state = state; }

    public int getBetAmount() { return betAmount; }
    public void setBetAmount(int betAmount) { this.betAmount = betAmount; }

    public ConcurrentHashMap<Integer, Integer> getPlayers() { return players; }
    public void setPlayers(ConcurrentHashMap<Integer, Integer> players) { this.players = players; }

    public Set<Integer> getGames() { return games; }
    public void setGames(Set<Integer> games) { this.games = games; }

    //endregion

    @Override
    public String toString() {
        return "RoomDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state=" + state +
                ", betAmount=" + betAmount +
                ", players=" + players +
                ", games=" + games +
                '}';
    }

}
