package cpe.asi.g12.common.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "market", url = "http://localhost:8083")//, url = "${service.market.address}")
public interface MarketServiceProxy {

	static final String baseURL = "/market";
	
	//==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();
    
  //==================================================================================================================
    
    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/buyCard/user/{idUser}/card/{idCard}")
    ResponseEntity<Boolean> buyCard(@PathVariable int idCard, @PathVariable int idUser);
    
    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/sellCard/user/{idUser}/card/{idCard}")
    ResponseEntity<Boolean> sellCard(@PathVariable int idCard, @PathVariable int idUser);
}
