package cpe.asi.g12.common.api;

import cpe.asi.g12.common.dto.GameDTO;
import cpe.asi.g12.common.dto.RoomDTO;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "games", url = "http://localhost:8086")//, url = "${service.market.address}")
public interface GameServiceProxy {

    static final String baseURL = "/games";

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();
    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL)
    List<RoomDTO> getGames();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}")
    ResponseEntity<GameDTO> getGame(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/id/{id}")
    ResponseEntity<GameDTO> getGameByID(@PathVariable int id);

    //==================================================================================================================

//    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/{number}")
//    Set<GameDTO> getRandomGame(@PathVariable int number);
//
//    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/generate")
//    List<GameDTO> generateGames();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL)
    ResponseEntity<GameDTO> addGame(@RequestBody RoomDTO room);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{id}")
    ResponseEntity<Boolean> removeGame(@PathVariable int id);

    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/{id}")
    ResponseEntity<UpdateResponseDTO<GameDTO>> updateGame(@PathVariable int id, @RequestBody GameDTO game);

    //==================================================================================================================
}
