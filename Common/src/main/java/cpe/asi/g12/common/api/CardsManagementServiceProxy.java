package cpe.asi.g12.common.api;

import cpe.asi.g12.common.dto.CardDTO;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Set;

@FeignClient(name = "cardsmanagement", url = "http://localhost:8084")//, url = "${service.cardsmanagement.address}")
public interface CardsManagementServiceProxy
{
    static final String baseURL = "/cards";

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//name")
    String getName();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "//names")
    String getNames();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL)
    List<CardDTO> getCards();

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/{id}")
    ResponseEntity<CardDTO> getCard(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/id/{id}")
    ResponseEntity<CardDTO> getCardByID(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/name/{name}")
    ResponseEntity<CardDTO> getCardByName(@PathVariable String name);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/{number}")
    Set<CardDTO> getRandomCard(@PathVariable int number);

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/random/generate")
    List<CardDTO> generateCards();

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL)
    ResponseEntity<CardDTO> addCard(@RequestBody CardDTO card);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{id}")
    ResponseEntity<Boolean> removeCard(@PathVariable int id);

    @RequestMapping(method = RequestMethod.PUT, value = baseURL + "/{id}")
    ResponseEntity<UpdateResponseDTO<CardDTO>> updateCard(@PathVariable int id, @RequestBody CardDTO card);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.GET, value = baseURL + "/user/{id}")
    Set<CardDTO> getUserCards(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value =  baseURL + "/{cardID}/user/{userID}")
    ResponseEntity<Boolean> getUserCard(@PathVariable int userID, @PathVariable int cardID);

    //==================================================================================================================

    @RequestMapping(method = RequestMethod.POST, value = baseURL + "/{cardID}/user/{userID}")
    ResponseEntity<CardDTO> addUserCard(@PathVariable int userID, @PathVariable int cardID);

    @RequestMapping(method = RequestMethod.DELETE, value = baseURL + "/{cardID}/user/{userID}")
    ResponseEntity<Boolean> removeUserCard(@PathVariable int userID, @PathVariable int cardID);

    //==================================================================================================================
}

/*
    @RequestMapping(method = RequestMethod.GET, value = "/cards")
    public List<CardDTO> getCards();

    @RequestMapping(method = RequestMethod.GET, value = "/cards/{param}")
    public ResponseEntity<CardDTO> getCard(@PathVariable String param);

    @RequestMapping(method = RequestMethod.GET, value = "/cards/random/{number}")
    public Set<CardDTO> getRandomCard(@PathVariable int number);

    @RequestMapping(method = RequestMethod.GET, value = "/cards/random")
    public void generateCards();

    @RequestMapping(method = RequestMethod.POST, value = "/cards")
    public ResponseEntity<CardDTO> addCard(@RequestBody CardDTO card);

    @RequestMapping(method = RequestMethod.DELETE, value = "/cards/{id}")
    public boolean removeCard(@PathVariable int id);

    @RequestMapping(method = RequestMethod.PATCH, value = "/cards/{id}")
    public ResponseEntity<CardDTO> patchCard(@PathVariable int id, @RequestBody CardDTO card);


    @RequestMapping(method = RequestMethod.PUT, value = "/cards/{id}")
    public ResponseEntity<CardDTO> putCard(@PathVariable int id, @RequestBody CardDTO card);

    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}/cards")
    public Set<CardDTO> getUserCards(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = "/users/{userID}/cards/{cardID}")
    public ResponseEntity<CardDTO> getUserCard(@PathVariable int userID, @PathVariable int cardID);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{userID}/cards/{cardID}")
    public ResponseEntity<CardDTO> addUserCard(@PathVariable int userID, @PathVariable int cardID);


    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userID}/cards/{cardID}")
    public boolean removeUserCard(@PathVariable int userID, @PathVariable int cardID);


    @RequestMapping(method = RequestMethod.POST, value = "/buy/users/{userID}/cards/{cardID}")
    public ResponseEntity<?> buyUserCard(@PathVariable int userID, @PathVariable int cardID);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sell/users/{userID}/cards/{cardID}")
    public boolean sellUserCard(@PathVariable int userID, @PathVariable int cardID);
 */