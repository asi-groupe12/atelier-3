package cpe.asi.g12.common.dto;

public class UpdateResponseDTO<ClassDTO>  //extends MutablePair<ClassDTO,ClassDTO>
{

	public UpdateResponseDTO() {
		
	}

	private ClassDTO beforeUpdate;
	private ClassDTO afterUpdate;
	
	public ClassDTO getBeforeUpdate() {
		return beforeUpdate;
	}

	public void setBeforeUpdate(ClassDTO beforeUpdate) {
		this.beforeUpdate = beforeUpdate;
	}

	public ClassDTO getAfterUpdate() {
		return afterUpdate;
	}

	public void setAfterUpdate(ClassDTO afterUpdate) {
		this.afterUpdate = afterUpdate;
	}

	public UpdateResponseDTO(ClassDTO beforeUpdate, ClassDTO afterUpdate) {
//		super(beforeUpdate,afterUpdate);
		this.beforeUpdate = beforeUpdate;
		this.afterUpdate = afterUpdate;
	}
//	
	public ClassDTO getOriginalItem() {
		return getBeforeUpdate();
	}
	
	public ClassDTO getNewItem() {
		return getAfterUpdate();
	}
	
	
}
