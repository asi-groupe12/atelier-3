package cpe.asi.g12.common.dto;

import java.util.concurrent.ConcurrentHashMap;

public class GameDTO
{

    //region GameDTO - Attributes

    // GameDTO
    private Integer id;
    private ConcurrentHashMap<Integer, Integer> players;
    private int winner;

    //endregion

    /**
     * GameDTO - Empty constructor
     */
    public GameDTO()
    {
        this.id = -1;
        this.winner = -1;
        this.players = new ConcurrentHashMap<>();
    }

    //region GameDTO - Getters & Setters

    public Integer getID() { return id; }
    public void setID(Integer id) { this.id = id; }

    public ConcurrentHashMap<Integer, Integer> getPlayers() { return players; }
    public void setPlayers(ConcurrentHashMap<Integer, Integer> players) { this.players = players; }

    public int getWinner() { return winner; }
    public void setWinner(int winner) { this.winner = winner; }

    //endregion

    @Override
    public String toString() {
        return "GameDTO{" +
                "id=" + id +
                ", players=" + players +
                ", winner=" + winner +
                '}';
    }

}
