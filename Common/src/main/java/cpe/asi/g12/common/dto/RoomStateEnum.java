
package cpe.asi.g12.common.dto;

public enum RoomStateEnum
{
    LOBBY_OPEN,
    LOBBY_FULL,
    GAME_WAITING,
    GAME_PLAYING,
    GAME_END,
}