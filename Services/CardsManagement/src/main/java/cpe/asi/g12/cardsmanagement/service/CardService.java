package cpe.asi.g12.cardsmanagement.service;

import java.util.*;

import cpe.asi.g12.common.dto.UpdateResponseDTO;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import cpe.asi.g12.cardsmanagement.model.Card;
import cpe.asi.g12.cardsmanagement.model.User;
import cpe.asi.g12.cardsmanagement.repository.CardRepository;
import cpe.asi.g12.cardsmanagement.repository.UserRepository;
import cpe.asi.g12.cardsmanagement.tools.Utils;
import cpe.asi.g12.common.dto.CardDTO;


@Service
public class CardService
{
	private final static int DEFAULT_CARDS_NUMBER = 5;
	private final CardRepository cardRepository;
    private final UserRepository userRepository;

    @Autowired
    public CardService(CardRepository cardRepository, UserRepository userRepository)
    {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    public Optional<CardDTO> addCard(CardDTO card)
    {
    	CardDTO addedCard = cardRepository.save(new Card(card)).toDTO();
        return getCard(addedCard.getID());
    }

    public List<CardDTO> getCards()
    {
        Iterable<Card> cardsIt = cardRepository.findAll();
        List<CardDTO> cardsList = new ArrayList<>();
        cardsIt.forEach(card -> cardsList.add(card.toDTO()));

        return cardsList;
    }

    public Optional<CardDTO> getCard(int id)
    {
        return Optional.ofNullable(cardRepository.findById(id).get().toDTO());
    }

    public Optional<CardDTO> getCard(String name)
    {
        return Optional.ofNullable(cardRepository.findByName(name).get().toDTO());
    }

    // Todo find by name
    // public Card getCard(String name)

    public boolean removeCard(int id)
    {
        Optional<CardDTO> cardToRemove = this.getCard(id);
        if(cardToRemove.isPresent())
        {
            this.removeCard(cardToRemove.get());
            return true;
        }
        else
        {
            return false;
        }
    }

    private void removeCard(CardDTO card)
    {
        cardRepository.delete(new Card(card));
    }

    public Optional<UpdateResponseDTO<CardDTO>> updateCard(int id, CardDTO sourceCardDTO)
    {
        Optional<CardDTO> cardToUpdate = this.getCard(id);
        if(cardToUpdate.isPresent())
        {
            CardDTO originalCardDTO = cardToUpdate.get();
            CardDTO updatedCardDTO = cardToUpdate.get();
            updatedCardDTO = this.updateCardWithSourceCard(updatedCardDTO, sourceCardDTO);

            Card updatedCard = cardRepository.save(new Card(updatedCardDTO));
            return Optional.of(new UpdateResponseDTO<CardDTO>(originalCardDTO, updatedCard.toDTO()));
        }
        else { return Optional.empty(); }
    }

    private CardDTO updateCardWithSourceCard(CardDTO cardToUpdate, CardDTO sourceCard)
    {
        // TODO update name (with check in db) ??
        if(Utils.isNotEmpty(sourceCard.getDescription()))           { cardToUpdate.setDescription(sourceCard.getDescription()); }
        if(Utils.isNotEmpty(sourceCard.getFamily()))                { cardToUpdate.setFamily(sourceCard.getFamily()); }
        if(Utils.isNotEmpty(sourceCard.getImageURL()))              { cardToUpdate.setImageURL(sourceCard.getImageURL()); }
        if(Utils.isStrictlyPositiveInteger(sourceCard.getEnergy())) { cardToUpdate.setEnergy(sourceCard.getEnergy()); }
        if(Utils.isStrictlyPositiveInteger(sourceCard.getHealth())) { cardToUpdate.setHealth(sourceCard.getHealth()); }
        if(Utils.isStrictlyPositiveInteger(sourceCard.getAttack())) { cardToUpdate.setAttack(sourceCard.getAttack()); }
        if(Utils.isStrictlyPositiveInteger(sourceCard.getDefense())){ cardToUpdate.setDefense(sourceCard.getDefense()); }
        if(Utils.isStrictlyPositiveInteger(sourceCard.getPrice()))  { cardToUpdate.setPrice(sourceCard.getPrice()); }

        return cardToUpdate;
    }

    public Set<CardDTO> getRandomCard(int number)
    {
        List<CardDTO> allCards = getCards();
        Set<CardDTO> randomCards = Utils.getRandomCards(allCards, number);

        if (randomCards.size() == number) {
            return randomCards;
        } else {
            return new HashSet<>();
        }
    }

    public List<CardDTO> generateCards()
    {
        List<CardDTO> cardDTOs = new ArrayList<>();
        Random random = new Random();

        for(int i=0; i<10; i++)
        {
            CardDTO cardDTO = new CardDTO();
            cardDTO.setName(UUID.randomUUID().toString());
            cardDTO.setDescription("GeneratedDescription");
            cardDTO.setFamily("GeneratedFamily");
            cardDTO.setImageURL("https://picsum.photos/200/300");
            cardDTO.setEnergy(random.nextInt(100));
            cardDTO.setHealth(random.nextInt(100));
            cardDTO.setAttack(random.nextInt(100));
            cardDTO.setDefense(random.nextInt(100));
            cardDTO.setPrice(random.nextInt(500));

            Optional<CardDTO> insertedCardDTO = addCard(cardDTO);
            insertedCardDTO.ifPresent(cardDTOs::add);
        }

        return cardDTOs;
    }

    public Set<CardDTO> getUserCards(int userID)
    {
    	Optional<User> user = this.checkIfUserExists(userID);
        Set<CardDTO> cards = new HashSet<>();

        if(user.isPresent())
        {
        	Iterable<Card> userCards = user.get().getCards();
        	userCards.forEach(card -> cards.add(card.toDTO()));
        }

        return cards;
    }

    public Pair<HttpStatus, Optional<CardDTO>> addUserCard(int userID, int cardID)
    {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;

        Optional<User> user = this.checkIfUserExists(userID);
        Optional<CardDTO> card = Optional.ofNullable(cardRepository.findById(cardID).get().toDTO());

        if(user.isPresent() && card.isPresent())
        {
        	if(user.get().getCards().contains(new Card(card.get())))
        	{
                // @see https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists
        	    httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            }
            else
            {
            	User modifiedUser = user.get();
                modifiedUser.addCard(new Card(card.get()));
                userRepository.save(modifiedUser);

                httpStatus = HttpStatus.OK;
            }
            
        }

        return new MutablePair<>(httpStatus, card);
    }

    public boolean getUserCard(int userID, int cardID)
    {
    	Optional<User> user = this.checkIfUserExists(userID);
        Optional<CardDTO> card = Optional.ofNullable(cardRepository.findById(cardID).get().toDTO());

        if(user.isPresent() && card.isPresent())
        {
            if(user.get().getCards().contains(new Card(card.get())))
            {
                return true;
            }
        }

        return false;
    }

    public boolean removeUserCard(int userID, int cardID)
    {
    	Optional<User> user = this.checkIfUserExists(userID);
        Optional<CardDTO> card = Optional.ofNullable(cardRepository.findById(cardID).get().toDTO());

        if(user.isPresent() && card.isPresent())
        {
            User modifiedUser = user.get();
            modifiedUser.removeCard(new Card(card.get()));
            userRepository.save(modifiedUser);

            return true;
        }

        return false;
    }
    
    private User addUser(User user){
    	Set<Card> randomCards = new HashSet<>();
    	Set<CardDTO> randomCardsDTO = Utils.getRandomCards(getCards(), DEFAULT_CARDS_NUMBER);
    	randomCardsDTO.forEach(cardDTO -> randomCards.add(new Card(cardDTO)));
    	user.setCards(randomCards);
    	userRepository.save(user);
    	return user;
    }
    
    private Optional<User> checkIfUserExists(int userID){
    	Optional<User> user = userRepository.findById(userID);
    	
    	if(user.isPresent()){
    		return user;
    	}else{
    		User userAdded = this.addUser(new User(userID));
    		return Optional.ofNullable(userAdded);
    	}
    }

}
