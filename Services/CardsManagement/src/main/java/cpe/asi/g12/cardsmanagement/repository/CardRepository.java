package cpe.asi.g12.cardsmanagement.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cpe.asi.g12.cardsmanagement.model.Card;

public interface CardRepository extends CrudRepository<Card, Integer>
{

    public Optional<Card> findByName(String name);
}