package cpe.asi.g12.cardsmanagement.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import cpe.asi.g12.common.dto.UserDTO;


@Entity
@Table(name = "app_user")
public class User {
	
	@Id
	private Integer id;
    
    @ManyToMany
    private Set<Card> cards;
	
	public User() { this.id = -1; }
	public User(Integer id) {
		this.id = id;
	}
	public User (UserDTO userDTO){
		this.id = userDTO.getID();
	}
	
	public User(Integer id, Set<Card> cards) {
		super();
		this.id = id;
		this.cards = cards;
	}
	
	public Integer getID() {
		return id;
	}
	public void setID(Integer id) {
		this.id = id;
	}
	
	public Set<Card> getCards() { return cards; }
    public void setCards(Set<Card> cards) { this.cards = cards; }
    public void addCard(Card card) { this.cards.add(card); }
    public void removeCard(Card card) { this.cards.remove(card); }

	public UserDTO toDTO ()
	{
		UserDTO userDTO = new UserDTO();
		userDTO.setID(this.id);
		return userDTO;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", cards=" + cards +
				'}';
	}
}
