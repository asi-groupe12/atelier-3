package cpe.asi.g12.cardsmanagement.controller;

import java.util.*;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import cpe.asi.g12.common.api.CardsManagementServiceProxy;
import cpe.asi.g12.common.dto.UpdateResponseDTO;

import cpe.asi.g12.cardsmanagement.service.CardService;
import cpe.asi.g12.common.dto.CardDTO;


@RestController
public class CardController implements CardsManagementServiceProxy {
    private final CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @Override
    public String getName() {
        return "cardsmanagement";
    }

    @Override
    public String getNames() { return getName(); }

    @Override
    public List<CardDTO> getCards() { return cardService.getCards(); }

    @Override
    public ResponseEntity<CardDTO> getCard(int id) { return ResponseEntity.of(cardService.getCard(id)); }

    @Override
    public ResponseEntity<CardDTO> getCardByID(int id) { return ResponseEntity.of(cardService.getCard(id)); }

    @Override
    public ResponseEntity<CardDTO> getCardByName(String name) { return ResponseEntity.of(cardService.getCard(name)); }

    @Override
    public Set<CardDTO> getRandomCard(int number) { return cardService.getRandomCard(number); }

    @Override
    public List<CardDTO> generateCards() { return cardService.generateCards(); }

    @Override
    public ResponseEntity<CardDTO> addCard(CardDTO card) { return ResponseEntity.of(cardService.addCard(card)); }

    public ResponseEntity<Boolean> removeCard(int id) { return ResponseEntity.of(Optional.of(cardService.removeCard(id))); }

    public ResponseEntity<UpdateResponseDTO<CardDTO>> updateCard(int id, CardDTO card) { return ResponseEntity.of(cardService.updateCard(id, card)); }

    public Set<CardDTO> getUserCards(int id) { return cardService.getUserCards(id); }

    public ResponseEntity<Boolean> getUserCard(int userID, int cardID) { return ResponseEntity.of(Optional.of(cardService.getUserCard(userID, cardID))); }

    public ResponseEntity<CardDTO> addUserCard(int userID, int cardID)
    {
        Pair<HttpStatus, Optional<CardDTO>> response = cardService.addUserCard(userID, cardID);
        return new ResponseEntity<CardDTO>(response.getValue().get(), response.getKey());
    }

    public ResponseEntity<Boolean> removeUserCard(int userID, int cardID) { return ResponseEntity.of(Optional.of(cardService.removeUserCard(userID, cardID))); }
}