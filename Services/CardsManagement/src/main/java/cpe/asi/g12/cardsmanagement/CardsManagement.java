package cpe.asi.g12.cardsmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardsManagement
{
    public static void main(String[] args)
    {
        SpringApplication.run(CardsManagement.class,args);
    }
}