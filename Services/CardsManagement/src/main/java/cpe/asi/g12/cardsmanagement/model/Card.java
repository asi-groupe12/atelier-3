package cpe.asi.g12.cardsmanagement.model;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;

import cpe.asi.g12.cardsmanagement.tools.Utils;
import cpe.asi.g12.common.dto.CardDTO;

@Entity
@Table(name = "app_card")
public class Card
{

    //region Card - Attributes

    @Id
    @GeneratedValue
    private Integer id;
    //
    @Column(unique = true)
    private String name;
    //
    private String description;
    private String family;
    private String imageURL;
    //
    private int energy;
    private int health;
    private int attack;
    private int defense;
    //
    private int price;

    //endregion

    //region Card - Constructors

    /**
     * Card - Empty constructor
     */
    public Card()
    {
        this(-1, "", "", "", "", -1, -1, -1, -1, -1);
    }

    /**
     * Card - Full constructor without ID
     */
    public Card(String name, String description, String family, String imageURL, int energy, int health, int attack, int defense, int price)
    {
        this(-1, name, description, family, imageURL, energy, health, attack, defense, price);
    }

    /**
     * Card - Full constructor
     */
    public Card(int id, String name, String description, String family, String imageURL, int energy, int health, int attack, int defense, int price)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.family = family;
        this.imageURL = imageURL;
        this.energy = energy;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.price = price;
    }
    
    public CardDTO toDTO(){
    	CardDTO cardDTO = new CardDTO();
    	cardDTO.setID(this.id);
    	cardDTO.setName(this.name);
    	cardDTO.setDescription(this.description);
    	cardDTO.setFamily(this.family);
    	cardDTO.setImageURL(this.imageURL);
    	cardDTO.setEnergy(this.energy);
    	cardDTO.setHealth(this.health);
    	cardDTO.setAttack(this.attack);
    	cardDTO.setDefense(this.defense);
    	cardDTO.setPrice(this.price);
    	return cardDTO;
    }
    
    public Card(CardDTO cardDTO){
    	this.id = cardDTO.getID();
    	this.name = cardDTO.getName();
    	this.description = cardDTO.getDescription();
    	this.family = cardDTO.getFamily();
    	this.imageURL = cardDTO.getImageURL();
    	this.energy = cardDTO.getEnergy();
    	this.health = cardDTO.getHealth();
    	this.attack = cardDTO.getAttack();
    	this.defense = cardDTO.getDefense();
    	this.price = cardDTO.getPrice();
    }

    //endregion

    //region Card - Getters & Setters

    public Integer getID() { return id; }
    //public void setID(Integer id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getFamily() { return family; }
    public void setFamily(String family) { this.family = family; }

    public String getImageURL() { return imageURL; }
    public void setImageURL(String imageURL) { this.imageURL = imageURL; }

    public int getEnergy() { return energy; }
    public void setEnergy(int energy) { this.energy = energy; }

    public int getHealth() { return health; }
    public void setHealth(int health) { this.health = health; }

    public int getAttack() { return attack; }
    public void setAttack(int attack) { this.attack = attack; }

    public int getDefense() { return defense; }
    public void setDefense(int defense) { this.defense = defense; }

    public int getPrice() { return price; }
    public void setPrice(int price) { this.price = price; }

    //endregion
 
	@Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", family='" + family + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", energy=" + energy +
                ", health=" + health +
                ", attack=" + attack +
                ", defense=" + defense +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(id, card.id) || Objects.equals(name, card.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

//  Alternative to equals/contains
//
//    public boolean isInCollection(Collection<Card> mapsCollection)
//    {
//        for(Card card : mapsCollection)
//        {
//            if(card.id.equals(this.id) || card.name.equals(this.name))
//            {
//                return true;
//            }
//        }
//        return false;
//    }

	

	
	
}
