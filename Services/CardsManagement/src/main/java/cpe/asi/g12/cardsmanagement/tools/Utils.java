package cpe.asi.g12.cardsmanagement.tools;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cpe.asi.g12.common.dto.CardDTO;

public class Utils {

    public static boolean isNotEmpty(String value)
    {
        return value != null && !value.isEmpty();
    }

    public static boolean isStrictlyPositiveInteger(int value)
    {
        return value > 0;
    }
    
    public static Set<CardDTO> getRandomCards(List<CardDTO> allCards, int number){
    	
    	Random random = new Random();
        Set<CardDTO> randomCards = new HashSet<>();
        int nbCards = allCards.size();
        for(int i=1; i<=number; i++)
        {
        	CardDTO selectedCard = allCards.get(random.nextInt(nbCards - i));
            if(selectedCard != null)
            {
                allCards.remove(selectedCard);
                randomCards.add(selectedCard);
            }
        }
        
        return randomCards;
    }

}
