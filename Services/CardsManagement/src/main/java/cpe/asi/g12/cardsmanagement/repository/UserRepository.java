package cpe.asi.g12.cardsmanagement.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cpe.asi.g12.cardsmanagement.model.User;


public interface UserRepository extends CrudRepository<User, Integer>
{

}
