package cpe.asi.g12.cardsmanagement.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import cpe.asi.g12.cardsmanagement.model.Card;
import cpe.asi.g12.cardsmanagement.model.User;
import cpe.asi.g12.cardsmanagement.repository.CardRepository;
import cpe.asi.g12.cardsmanagement.repository.UserRepository;
import cpe.asi.g12.common.dto.CardDTO;
import cpe.asi.g12.common.dto.UpdateResponseDTO;


@RunWith(SpringRunner.class)
@WebMvcTest(value = CardService.class)
public class CardServiceTest {

	@Autowired
	private CardService cardService;

	@MockBean
	private CardRepository cardRepository;
	@MockBean
    private UserRepository userRepository;
	
	private Card card;
	private User user;
	private CardDTO cardDTO;
	private List<Card> cards = new ArrayList<>();
	private List<CardDTO> cardsDTO = new ArrayList<>();
	
	@Before
    public void setUp() {
		card = new Card(1, "card1", "descr1", "fam1", "img1", 10, 10, 10, 10, 100);
		cardDTO = card.toDTO();
		cardsDTO.add(cardDTO);
		cards.add(card);
    }

	@Test
	public void getCards() {
		Mockito.when(
				cardRepository.findAll()
				).thenReturn(cards);
		List<CardDTO> myCards = cardService.getCards();
		assertTrue(myCards.toString().equals(cardsDTO.toString()));
	}
	
	@Test
	public void getCardById() {
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Optional<CardDTO> myCard = cardService.getCard(card.getID());
		assertTrue(myCard.get().getID().equals(card.getID()));
	}
	
	@Test
	public void getCardByName() {
		Mockito.when(
				cardRepository.findByName(Mockito.anyString())
				).thenReturn(Optional.ofNullable(card));
		Optional<CardDTO> myCard = cardService.getCard(card.getName());
		assertTrue(myCard.get().getName().equals(card.getName()));
	}
	
	@Test
	public void addCard() {
		Mockito.when(
				cardRepository.save(Mockito.any(Card.class))
				).thenReturn(card);
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Optional<CardDTO> myCardDTO = cardService.addCard(cardDTO);
		assertTrue(myCardDTO.get().toString().equals(cardDTO.toString()));
	}
	
	@Test
	public void removeCardOk() {
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		boolean removedCard = cardService.removeCard(card.getID());
		assertTrue(removedCard==true);
	}
	
	@Test
	public void updateCard() {
		CardDTO sourceCardDTO = new Card(1, "card98", "descr78", "fam1", "img1", 10, 10, 10, 10, 100).toDTO();
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Mockito.when(
				cardRepository.save(Mockito.any(Card.class))
				).thenReturn(new Card(sourceCardDTO));
		Optional<UpdateResponseDTO<CardDTO>> updatedCard = cardService.updateCard(card.getID(), sourceCardDTO);
		//assertTrue(removedCard==false);
	}
	
	@Test
	public void getRandomCard() {
		List<Card> myList = new ArrayList<>();
		for (int i=0;i<4;i++){
			myList.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Mockito.when(
				cardRepository.findAll()
				).thenReturn(myList);
		Set<CardDTO> myRandomList = cardService.getRandomCard(2);
		assertTrue(myRandomList.size()==2);
	}
	
	@Test
	public void generateCards() {
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Mockito.when(
				cardRepository.save(Mockito.any(Card.class))
				).thenReturn(card);
		List<CardDTO> myList = cardService.generateCards();
		assertTrue(myList.size()==10);
	}
	
	@Test
	public void getUserCards() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=0;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findAll()
				).thenReturn(userCards);
		Set<CardDTO> myList = cardService.getUserCards(1);
		assertTrue(myList.size()==4);
	}
	
	@Test
	public void userHasTheCard() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=0;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		boolean userHasCard = cardService.getUserCard(1,1);
		assertTrue(userHasCard==true);
	}
	
	@Test
	public void userHasNotCard() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=2;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		boolean userHasCard = cardService.getUserCard(1,1);
		assertTrue(userHasCard==false);
	}
	
	@Test
	public void removeUserCardOK() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=0;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		boolean cardIsRemoved = cardService.removeUserCard(1,1);
		assertTrue(cardIsRemoved==true);
	}
	
	@Test
	public void addUserCardOK() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=2;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Pair<HttpStatus, Optional<CardDTO>> cardIsAdded = cardService.addUserCard(1,1);
		assertTrue(cardIsAdded.getKey().equals(HttpStatus.OK));
	}
	
	@Test
	public void userHasAlreadyTheCard() {
		user = new User(1);
		List<Card> userCards = new ArrayList<>();
		for (int i=0;i<4;i++){
			userCards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100));
		}
		Set<Card> userCardsSet = new HashSet<>(userCards);
		user.setCards(userCardsSet);
		Mockito.when(
				userRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(user));
		Mockito.when(
				cardRepository.findById(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(card));
		Pair<HttpStatus, Optional<CardDTO>> cardIsAdded = cardService.addUserCard(1,1);
		assertTrue(cardIsAdded.getKey().equals(HttpStatus.UNPROCESSABLE_ENTITY));
	}
}
