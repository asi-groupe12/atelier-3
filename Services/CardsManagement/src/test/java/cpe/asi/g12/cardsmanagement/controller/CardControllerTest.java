package cpe.asi.g12.cardsmanagement.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cpe.asi.g12.cardsmanagement.model.Card;
import cpe.asi.g12.cardsmanagement.service.CardService;
import cpe.asi.g12.common.dto.CardDTO;


@RunWith(SpringRunner.class)
@WebMvcTest(value = CardController.class)
public class CardControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CardService cardService;
	
	@Test
	public void getCards() throws Exception {
		List<CardDTO> cards = new ArrayList<>();
		for (int i=0;i<4;i++){
			cards.add(new Card(1, "card"+i, "descr"+i, "fam"+i, "img"+i, 10, 10, 10, 10, 100).toDTO());
		}
		String expectedResult = "[{\"id\":1,\"name\":\"card0\",\"description\":\"descr0\""
				+ ",\"family\":\"fam0\",\"imageURL\":\"img0\",\"energy\":10,\"health\":10"
				+ ",\"attack\":10,\"defense\":10,\"price\":100},{\"id\":1,\"name\":\"card1\""
				+ ",\"description\":\"descr1\",\"family\":\"fam1\",\"imageURL\":\"img1\""
				+ ",\"energy\":10,\"health\":10,\"attack\":10,\"defense\":10,\"price\":100}"
				+ ",{\"id\":1,\"name\":\"card2\",\"description\":\"descr2\",\"family\":\"fam2\""
				+ ",\"imageURL\":\"img2\",\"energy\":10,\"health\":10,\"attack\":10,\"defense\":10"
				+ ",\"price\":100},{\"id\":1,\"name\":\"card3\",\"description\":\"descr3\""
				+ ",\"family\":\"fam3\",\"imageURL\":\"img3\",\"energy\":10,\"health\":10"
				+ ",\"attack\":10,\"defense\":10,\"price\":100}]";
		Mockito.when(
				cardService.getCards()
				).thenReturn(cards);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(expectedResult, result.getResponse().getContentAsString());

	}
	
	@Test
	public void getCard() throws Exception {
		CardDTO card = card = new Card(1, "card1", "descr1", "fam1", "img1", 10, 10, 10, 10, 100).toDTO();
		
		String expectedResult = "{\"id\":1,\"name\":\"card1\""
				+ ",\"description\":\"descr1\",\"family\":\"fam1\",\"imageURL\":\"img1\""
				+ ",\"energy\":10,\"health\":10,\"attack\":10,\"defense\":10,\"price\":100}";
		
		Mockito.when(
				cardService.getCard(1)
				).thenReturn(Optional.ofNullable(card));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(expectedResult, result.getResponse().getContentAsString());

	}

	
}
