package cpe.asi.g12.market.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import cpe.asi.g12.common.api.UserDataServiceProxy;
import cpe.asi.g12.common.dto.CardDTO;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;
import cpe.asi.g12.common.api.CardsManagementServiceProxy;

@Service
public class MarketService
{
    private final UserDataServiceProxy userDataServiceProxy;
    private final CardsManagementServiceProxy cardsManagementServiceProxy;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public MarketService(UserDataServiceProxy userDataServiceProxy, CardsManagementServiceProxy cardsManagementServiceProxy)
    {
        this.cardsManagementServiceProxy = cardsManagementServiceProxy;
        this.userDataServiceProxy = userDataServiceProxy;
    }

    public String getProxiesNames() { return userDataServiceProxy.getName() + System.lineSeparator() + cardsManagementServiceProxy.getName(); }

    /**
     * Achat de cartes
     *
     * @param idCard
     * @param idUser
     * @return
     * @throws JsonProcessingException 
     */
    public boolean buyCard(int idCard, int idUser) {
    	
        ResponseEntity<UserDTO> userResponse = userDataServiceProxy.getUserByID(idUser);
        ResponseEntity<CardDTO> cardResponse = cardsManagementServiceProxy.getCardByID(idCard);
        
        if (userResponse.getStatusCode() == HttpStatus.OK && userResponse.getBody() != null && cardResponse.getStatusCode() == HttpStatus.OK && cardResponse.getBody() != null) {
        	UserDTO user = userResponse.getBody();
            CardDTO card = cardResponse.getBody();
            System.out.println(user);
            System.out.println(card);
            ResponseEntity<Boolean> isCardAlreadyBought = cardsManagementServiceProxy.getUserCard(user.getID(), card.getID());
            if (isCardAlreadyBought.getStatusCode() == HttpStatus.OK && isCardAlreadyBought.getBody() == false) {
                if (user.getBalance() >= card.getPrice()) {
                    int newBalance = user.getBalance() - card.getPrice();
                    UserDTO userToUpdate = new UserDTO();
                    userToUpdate.setID(user.getID());
                    userToUpdate.setBalance(newBalance);
                    System.out.println("\nUSER TO UPDATE => "+userToUpdate);
                    ResponseEntity<UpdateResponseDTO<UserDTO>> updateUserResponse = userDataServiceProxy.updateUser(userToUpdate.getID(), userToUpdate);
                    System.out.println("\nREPONSE UPDATE USER => "+updateUserResponse);
                    if (updateUserResponse.getStatusCode() == HttpStatus.OK && updateUserResponse.getBody() != null) {
                        UpdateResponseDTO<UserDTO> users = updateUserResponse.getBody();
                        System.out.println(users.getOriginalItem());
                        System.out.println(users.getNewItem());
                        if (card.getPrice() == (users.getOriginalItem().getBalance() - users.getNewItem().getBalance())) {
                            ResponseEntity<CardDTO> updateCardResponse = cardsManagementServiceProxy.addUserCard(userToUpdate.getID(), card.getID());
                            if (updateCardResponse.getStatusCode() == HttpStatus.OK && updateCardResponse.getBody() != null) {
                                return true;
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    /**
     * Vente de cartes
     *
     * @param idCard
     * @param idUser
     * @return
     * @throws JsonProcessingException 
     */
    public boolean sellCard(int idCard, int idUser) 
    {
        ResponseEntity<UserDTO> userResponse = userDataServiceProxy.getUserByID(idUser);
        ResponseEntity<CardDTO> cardResponse = cardsManagementServiceProxy.getCardByID(idCard);

        if (userResponse.getStatusCode() == HttpStatus.OK && userResponse.getBody() != null && cardResponse.getStatusCode() == HttpStatus.OK && cardResponse.getBody() != null)
        {
            UserDTO user = userResponse.getBody();
            CardDTO card = cardResponse.getBody();
            System.out.println(user);
            System.out.println(card);

            ResponseEntity<Boolean> isCardAlreadyBought = cardsManagementServiceProxy.getUserCard(user.getID(), card.getID());
            if (isCardAlreadyBought.getStatusCode() == HttpStatus.OK && isCardAlreadyBought.getBody() == true)
            {
                int newBalance = user.getBalance() + card.getPrice();
                UserDTO userToUpdate = new UserDTO();
                userToUpdate.setID(user.getID());
                userToUpdate.setBalance(newBalance);
                ResponseEntity<UpdateResponseDTO<UserDTO>> updateUserResponse = userDataServiceProxy.updateUser(userToUpdate.getID(), userToUpdate);

                if (updateUserResponse.getStatusCode() == HttpStatus.OK && updateUserResponse.getBody() != null)
                {
                    UpdateResponseDTO<UserDTO> users = updateUserResponse.getBody();
                    System.out.println(users.getOriginalItem());
                    System.out.println(users.getNewItem());
                    if (card.getPrice() == (users.getNewItem().getBalance() - users.getOriginalItem().getBalance()))
                    {
                        ResponseEntity<Boolean> updateCardResponse = cardsManagementServiceProxy.removeUserCard(userToUpdate.getID(), card.getID());

                        if (updateCardResponse.getStatusCode() == HttpStatus.OK && updateCardResponse.getBody() != null)
                        {
                            if (updateCardResponse.getBody() == true)
                            {
                                return true;

                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}
