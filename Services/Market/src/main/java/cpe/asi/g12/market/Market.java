package cpe.asi.g12.market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackageClasses = { cpe.asi.g12.common.api.CardsManagementServiceProxy.class })
public class Market
{
    public static void main(String[] args)
    {
        SpringApplication.run(Market.class,args);
    }
}