package cpe.asi.g12.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import cpe.asi.g12.common.api.MarketServiceProxy;
import cpe.asi.g12.market.service.MarketService;

import java.util.Optional;


@RestController
public class MarketRest implements MarketServiceProxy
{

	private final MarketService marketService;
	
	@Autowired
    public MarketRest(MarketService marketService) {
        this.marketService = marketService;
    }
	
	public String getName() { return "market"; }

    public String getNames() { return getName() + System.lineSeparator() + marketService.getProxiesNames();}
	
	@Override
	public ResponseEntity<Boolean> buyCard(int idCard, int idUser) { return ResponseEntity.of(Optional.of(marketService.buyCard(idCard,idUser))); }

	@Override
	public ResponseEntity<Boolean> sellCard(int idCard, int idUser) { return ResponseEntity.of(Optional.of(marketService.sellCard(idCard,idUser))); }
		
	
}
