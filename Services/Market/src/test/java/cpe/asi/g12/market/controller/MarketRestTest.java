package cpe.asi.g12.market.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cpe.asi.g12.market.service.MarketService;


@RunWith(SpringRunner.class)
@WebMvcTest(value = MarketRest.class)
public class MarketRestTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MarketService marketService;
	
	String mockName = "market";

	
	
	@Test
	public void getName() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/market//name").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertTrue(mockName.equals(result.getResponse()
				.getContentAsString()));
	}
	
}
