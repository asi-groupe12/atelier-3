package cpe.asi.g12.market.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import cpe.asi.g12.common.api.CardsManagementServiceProxy;
import cpe.asi.g12.common.api.UserDataServiceProxy;
import cpe.asi.g12.common.dto.CardDTO;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MarketService.class)
public class MarketServiceTest {
	@Autowired
	private MarketService marketService;
	
	@MockBean
	private UserDataServiceProxy userDataServiceProxy;
	
	@MockBean
	private CardsManagementServiceProxy cardsManagementServiceProxy;
	
	UserDTO userWithoutTheCard = new UserDTO();
	UserDTO userWithTheCard = new UserDTO();
	CardDTO cardBuyable = new CardDTO();
	CardDTO cardNotBuyable= new CardDTO();
	
	
	@Before
    public void setUp() {
	
		userWithoutTheCard.setID(1);
		userWithoutTheCard.setLogin("TestUserWithoutTheCard");
		userWithoutTheCard.setBalance(5000);
		
		userWithTheCard.setID(2);
		userWithTheCard.setLogin("TestUserWithTheCard");
		userWithTheCard.setBalance(5000);
		
		cardBuyable.setID(1);
		cardBuyable.setName("TestCardBuyable");
		cardBuyable.setPrice(4000);
		

		cardNotBuyable.setID(2);
		cardNotBuyable.setName("TestCardNotBuyable");
		cardNotBuyable.setPrice(6000);
		
		
		Mockito.when(
				userDataServiceProxy.getUserByID(1)
				).thenReturn(ResponseEntity.ok(userWithoutTheCard));
		
		Mockito.when(
				userDataServiceProxy.getUserByID(2)
				).thenReturn(ResponseEntity.ok(userWithTheCard));
		
		//La carte est possédé par l'User
		Mockito.when(
				cardsManagementServiceProxy.getUserCard(Mockito.eq(2), Mockito.anyInt() )
				).thenReturn(ResponseEntity.ok(true));
		
		//La carte n'est pas possédé par l'User
		Mockito.when(
				cardsManagementServiceProxy.getUserCard(Mockito.eq(1),Mockito.anyInt())
				).thenReturn(ResponseEntity.ok(false));
		
		Mockito.when(
				cardsManagementServiceProxy.getCardByID(1)
				).thenReturn(ResponseEntity.ok(cardBuyable));
		
		Mockito.when(
				cardsManagementServiceProxy.getCardByID(2)
				).thenReturn(ResponseEntity.ok(cardNotBuyable));
		
		
    }

	
	@Test
	public void buyCard() {
				
		UserDTO beforeUpdate = new UserDTO();
		beforeUpdate.setID(1);
		beforeUpdate.setLogin("TestUserWithoutTheCard");
		beforeUpdate.setBalance(5000);
		UserDTO afterUpdate = new UserDTO();
		afterUpdate.setID(1);
		afterUpdate.setLogin("TestUserWithoutTheCard");
		afterUpdate.setBalance(5000-cardBuyable.getPrice());
		Mockito.when(
				userDataServiceProxy.updateUser(Mockito.eq(1),Mockito.any(UserDTO.class))
				).thenReturn(ResponseEntity.ok(new UpdateResponseDTO<UserDTO>(beforeUpdate,afterUpdate)));

		Mockito.when(
				cardsManagementServiceProxy.addUserCard(Mockito.eq(1), Mockito.anyInt())
				).thenReturn(ResponseEntity.ok(cardBuyable));
		
		
		boolean cardBuyableAndUserWithoutTheCard= marketService.buyCard(1, 1);
		boolean cardBuyableAndUserWithTheCard= marketService.buyCard(1, 2);
		boolean cardNotBuyableAndUserWithoutTheCard= marketService.buyCard(2, 1);
		boolean cardNotBuyableAndUserWithTheCard= marketService.buyCard(2, 2);
		assertTrue(cardBuyableAndUserWithoutTheCard);
		assertFalse(cardBuyableAndUserWithTheCard);
		assertFalse(cardNotBuyableAndUserWithoutTheCard);
		assertFalse(cardNotBuyableAndUserWithTheCard);
	}
	
	
	@Test
	public void sellCArd() {
		
		UserDTO beforeUpdate = new UserDTO();
		beforeUpdate.setID(2);
		beforeUpdate.setLogin("TestUserWithTheCard");
		beforeUpdate.setBalance(5000);
		UserDTO afterUpdate = new UserDTO();
		afterUpdate.setID(2);
		afterUpdate.setLogin("TestUserWithTheCard");
		afterUpdate.setBalance(5000+cardBuyable.getPrice());
		
		
		Mockito.when(
				userDataServiceProxy.updateUser(Mockito.eq(2),Mockito.any(UserDTO.class))
				).thenReturn(ResponseEntity.ok(new UpdateResponseDTO<UserDTO>(beforeUpdate,afterUpdate)));
		
		Mockito.when(
				cardsManagementServiceProxy.removeUserCard(Mockito.eq(2), Mockito.anyInt())
				).thenReturn(ResponseEntity.ok(true));
				
				
		boolean cardBuyableAndUserWithoutTheCard = marketService.sellCard(1, 1);
		boolean cardBuyableAndUserWithTheCard = marketService.sellCard(1, 2);
		boolean cardNotBuyableAndUserWithoutTheCard = marketService.sellCard(2, 1);
		assertTrue(cardBuyableAndUserWithTheCard);
		assertFalse(cardBuyableAndUserWithoutTheCard);
		assertFalse(cardNotBuyableAndUserWithoutTheCard);
	}
}
