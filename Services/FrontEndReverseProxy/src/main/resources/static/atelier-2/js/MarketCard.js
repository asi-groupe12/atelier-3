if (sessionStorage.getItem("user") != null) {
    $(document).ready(function () {
            var user = getUser(sessionStorage.getItem("user"));
            var pathname = '/cards';
            $.ajax({
                url: pathname,
                type: 'GET',
                dataType: 'json',
                success: function (data, status) {
                    console.log(data);

                    for (i = 0; i < data.length; i++) {
                        addCardToList(data[i].id, data[i].family, data[i].imageURL, data[i].name, data[i].description, data[i].health, data[i].energy, data[i].attack, data[i].defense, data[i].price);
                    }
                    //Click Listeners sur les row afin d'afficher la "Card Selected"
                    addRowHandlers();
                },
                error: function (res, statut, erreur) {
                    console.log(res);
                }
            })
            
            // Ajout de la fonction sur le bouton "Buy"
            // Lorsqu'on clique sur le bouton du formulaire
//        	$("#button-buy-card").click(buyCard());
    });
}
else{
    document.location.href="./sign_in.html";
}


function buyCard(id){
	console.log("BUY CARD");
	const STATUS_OK = 200;
	const STATUS_CARD_ALREADY_ENROLLED = 226;
    var pathname = '/buy/users/'+sessionStorage.getItem("user")+'/cards/'+id;
    console.log(pathname);
    $.ajax({
        url: pathname,
        type: 'POST',
        dataType: 'json',
        success: function (data, status, xhr) {
        	if(xhr.status==STATUS_OK){
        		getUser(sessionStorage.getItem("user"));
            }
        	else if(xhr.status==STATUS_CARD_ALREADY_ENROLLED){
            	alert("Vous possédez déjà cette carte !");
            }
        },
        error: function (res, statut, erreur) {
        	console.log(res);
        	alert("Erreur lors de l'achat de la carte.");
        }
    })
}

function addCardToList(id, familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span class='cardName'>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0' onclick='buyCard("+id+")'>\
            <div class='hidden content'>Buy</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tr:last').after('<tr>'+content+'</tr>');
    
    
};

function addRowHandlers() {
    var table = document.getElementById("cardListId");
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = function(row) {
            return function() {
                var cell = row.getElementsByClassName("cardName")[0];
                var name = cell.innerText;
                displaySelectedCard(name);
            };
        };
        currentRow.onclick = createClickHandler(currentRow);
    }
};

function displaySelectedCard(name) {
    var pathname = "/cards/"+name;
    $.ajax({
        url: pathname,
        type:'GET',
        dataType:'json',
        success: function(data,status){
            console.log(data);
            //Remplissage de la carte
            $('#cardFamilyNameId')[0].innerText  = data.family;
            $('#cardImgId')[0].src               = data.imageURL;
            $('#cardNameId')[0].innerText        = data.name;
            $('#cardDescriptionId')[0].innerText = data.description;
            $('#cardHPId')[0].innerText          = data.health;
            $('#cardEnergyId')[0].innerText      = data.energy;
            $('#cardAttackId')[0].innerText      = data.attack;
            $('#cardDefenceId')[0].innerText     = data.defence;
            $('#cardPriceId')[0].innerText     = data.price;
            $('#button-buy-selected-card').attr('onclick', 'buyCard('+data.id+');');

            //Affichage de la carte
            $("#CardSelection" ).removeClass( "cardNotSelected" );
            $("#CardSelection" ).addClass( "cardSelected" );

        },
        error: function(res,statut,erreur){
            console.log(res);
        }
    })

};


function getUser(id){
    var pathname = "/users/"+id;
    $.ajax({
        url: pathname,
        type:'GET',
        dataType:'json',
        success: function(user,status){
            console.log(user);
            $('#userHeader')[0].innerText=user.surname;
            $('#moneyHeader')[0].innerText=user.money;
        },
        error: function(res,statut,erreur){
            console.log(res);
        }
    })
};