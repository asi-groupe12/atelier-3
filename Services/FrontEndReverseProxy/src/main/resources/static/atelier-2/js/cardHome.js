if (sessionStorage.getItem("user") != null){
    $(document).ready(function(){
        var user=getUser(sessionStorage.getItem("user"));

        $("#playButtonId").click(function(){
            //TO DO
        });
        $("#buyButtonId").click(function(){
            window.location.assign("./cardMarket.html");
        });
        $("#sellButtonId").click(function(){
            window.location.assign("./cardProfile.html");
        });


    });
}
else{
    document.location.href="./sign_in.html";
}

function getUser(id)
{
    var pathname = "/users/" + id;
    $.ajax({
        url: pathname,
        type:'GET',
        dataType:'json',
        success: function(user,status){
            console.log(user);
            $('#userHeader')[0].innerText=user.surname;
            $('#moneyHeader')[0].innerText=user.money;
        },
        error: function(res,statut,erreur){
            console.log(res);
        }
    })
};