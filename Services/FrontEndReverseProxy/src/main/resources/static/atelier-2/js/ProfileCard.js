if (sessionStorage.getItem("user") != null) {
    $(document).ready(function () {
        var id = sessionStorage.getItem("user");
        var user=getUser(id);
        var pathname = '/users/'+id+'/cards';
        getCards(id);
//        $.ajax({
//            url: pathname,
//            type: 'GET',
//            dataType: 'json',
//            success: function (data, status) {
//                console.log(data);
//                for (i = 0; i < data.length; i++) {
//                    addCardToList(data[i].id,data[i].family, data[i].imageURL, data[i].name, data[i].description, data[i].health, data[i].energy, data[i].attack, data[i].defense, data[i].price);
//                }
//                //Click Listeners sur les row afin d'afficher la "Card Selected"
//                addRowHandlers();
//            },
//            error: function (res, statut, erreur) {
//                console.log(res);
//            }
//        })

    });
}
else{
    document.location.href="./sign_in.html";
}

function getCards(userId){
	clearTable();
    var pathname = '/users/'+userId+'/cards';
    $.ajax({
        url: pathname,
        type: 'GET',
        dataType: 'json',
        success: function (data, status) {
        	
            console.log(data);
            for (i = 0; i < data.length; i++) {
                addCardToList(data[i].id,data[i].family, data[i].imageURL, data[i].name, data[i].description, data[i].health, data[i].energy, data[i].attack, data[i].defense, data[i].price);
            }
            //Click Listeners sur les row afin d'afficher la "Card Selected"
            addRowHandlers();
        },
        error: function (res, statut, erreur) {
            console.log(res);
        }
    })
}

function clearTable(){
	$(".cardItem").remove();
}


function sellCard(id){
	var userId = sessionStorage.getItem("user");
    var pathname = '/sell/users/'+userId+'/cards/'+id;
    console.log(pathname);
    $.ajax({
        url: pathname,
        type: 'DELETE',
        dataType: 'json',
        success: function (data, status) {
            console.log(data);
            getCards(userId);
            getUser(userId);
            alert("Carte Vendue !");
        },
        error: function (res, statut, erreur) {
            console.log(res);
        }
    })
}

function addCardToList(id,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span class='cardName'>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0' onclick='sellCard("+id+")'>\
            <div class='hidden content'>Sell</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tr:last').after('<tr class=\'cardItem\'>'+content+'</tr>');
//    $('#cardListId tr:last').after('<tr>'+content+'</tr>');
    
    
};

function addRowHandlers() {
    var table = document.getElementById("cardListId");
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = function(row) {
            return function() {
                var cell = row.getElementsByClassName("cardName")[0];
                var name = cell.innerText;
                displaySelectedCard(name);
            };
        };
        currentRow.onclick = createClickHandler(currentRow);
    }
};

function displaySelectedCard(name) {
    var pathname = "/cards/"+name;
    $.ajax({
        url: pathname,
        type:'GET',
        dataType:'json',
        success: function(data,status){
            console.log(data);
            //Remplissage de la carte
            $('#cardFamilyNameId')[0].innerText  = data.family;
            $('#cardImgId')[0].src               = data.imageURL;
            $('#cardNameId')[0].innerText        = data.name;
            $('#cardDescriptionId')[0].innerText = data.description;
            $('#cardHPId')[0].innerText          = data.health;
            $('#cardEnergyId')[0].innerText      = data.energy;
            $('#cardAttackId')[0].innerText      = data.attack;
            $('#cardDefenceId')[0].innerText     = data.defence;
            $('#cardPriceId')[0].innerText     = data.price;
            $('#button-sell-selected-card').attr('onclick', 'sellCard('+data.id+');');

            //Affichage de la carte
            $("#CardSelection" ).removeClass( "cardNotSelected" );
            $("#CardSelection" ).addClass( "cardSelected" );
            

        },
        error: function(res,statut,erreur){
            console.log(res);
        }
    })

};


function getUser(id){
    var pathname = "/users/"+id;
    $.ajax({
        url: pathname,
        type:'GET',
        dataType:'json',
        success: function(user,status){
            console.log(user);
            $('#userHeader')[0].innerText=user.surname;
            $('#moneyHeader')[0].innerText=user.money;
        },
        error: function(res,statut,erreur){
            console.log(res);
        }
    })
};

