//======================================================================================================================
//====================================================== IMPORTS =======================================================
//======================================================================================================================

import { MarketService }                from './utils/api-market-helper.js'
import { CardService }                  from './utils/api-cards-helper.js'
import { UserDataService }              from './utils/api-userdata-helper.js'
import { CARD_HOME }                    from './utils/web-pages-helper.js'
import { SessionHelper }                from "./utils/web-session-helper.js";

//======================================================================================================================
//===================================================== FUNCTIONS ======================================================
//======================================================================================================================

function clearTable(){
	$(".cardItem").remove();
}

function sellCard(id){
	MarketService.sellCard(SessionHelper.getUserID(), id, sellCardSuccess, sellCardError);
}

function sellCardSuccess(data){
	CardService.getCardsByUserId(SessionHelper.getUserID(), getCardsByUserIdSuccess, getCardsByUserIdError)
    getUser();
}

function sellCardError(res){
	console.log(res);
	loginErrorHandler(res, 'vente de la carte impossible. (erreur interne)');
}

function addCardToList(id, familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    
    let content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span class='cardName'>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0' onclick='buyCard("+id+")'>\
            <div class='hidden content'>Buy</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tr:last').after('<tr class=\'cardItem\'>'+content+'</tr>');
//    $('#cardListId tr:last').after('<tr>'+content+'</tr>');
    
    
};

function addRowHandlers() {
    var table = document.getElementById("cardListId");
    var rows = table.getElementsByTagName("tr");
    for (var i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = function(row) {
            return function() {
                var cell = row.getElementsByClassName("cardName")[0];
                var name = cell.innerText;
                CardService.getCardByName(name, displaySelectedCardSuccess, displaySelectedCardError);
            };
        };
        currentRow.onclick = createClickHandler(currentRow);
    }
};

function displaySelectedCardSuccess(data)
{
	console.log(data);
    //Remplissage de la carte
    $('#cardFamilyNameId')[0].innerText  = data.family;
    $('#cardImgId')[0].src               = data.imageURL;
    $('#cardNameId')[0].innerText        = data.name;
    $('#cardDescriptionId')[0].innerText = data.description;
    $('#cardHPId')[0].innerText          = data.health;
    $('#cardEnergyId')[0].innerText      = data.energy;
    $('#cardAttackId')[0].innerText      = data.attack;
    $('#cardDefenceId')[0].innerText     = data.defence;
    $('#cardPriceId')[0].innerText     = data.price;
    $('#button-buy-selected-card').attr('onclick', 'sellCard('+data.id+');');

    //Affichage de la carte
    $("#CardSelection" ).removeClass( "cardNotSelected" );
    $("#CardSelection" ).addClass( "cardSelected" );
    
}

function displaySelectedCardError(res)
{
    loginErrorHandler(res, 'affichage de la carte impossible.');
}


function getUser(){
	UserDataService.getUser(SessionHelper.getUsername(), getUserSuccess, getUserError);
};

function getUserSuccess(data)
{
	$('#userHeader')[0].innerText=data.login;
    $('#moneyHeader')[0].innerText=data.balance;
}

function getUserError(res)
{
    loginErrorHandler(res, 'récupération du profil impossible.');
}

function getCardsByUserIdSuccess(data)
{
	clearTable();
    console.log(data);
    for (var i = 0; i < data.length; i++) {
        addCardToList(data[i].id, data[i].family, data[i].imageURL, data[i].name, data[i].description, data[i].health, data[i].energy, data[i].attack, data[i].defense, data[i].price);
    }
    //Click Listeners sur les row afin d'afficher la "Card Selected"
    addRowHandlers();
}

function getCardsByUserIdError(res)
{
    loginErrorHandler(res, 'récupération des cartes impossible.');
}

//======================================================================================================================
//======================================================= SCRIPT =======================================================
//======================================================================================================================


if(SessionHelper.isUserLogged())
{
    $(document).ready(function () {
            getUser();
            CardService.getCardsByUserId(SessionHelper.getUserID(), getCardsByUserIdSuccess, getCardsByUserIdError)
    });
}
else{
    document.location.href="./sign_in.html";
}

//Enable functions for the document
window.buyCard = sellCard;
