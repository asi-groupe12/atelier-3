
//======================================================================================================================
//====================================================== IMPORTS =======================================================
//======================================================================================================================

import { AuthService }                  from './utils/api-auth-helper.js'
import { UserDataService }              from './utils/api-userdata-helper.js'
import { CARD_HOME }                    from './utils/web-pages-helper.js'
import { SessionHelper }                from "./utils/web-session-helper.js";

//======================================================================================================================
//===================================================== FUNCTIONS ======================================================
//======================================================================================================================

function loginSuccess(data)
{
    console.log(data);
    //
    if (data !== '' && data !== 'undefined')
    {
        if(data === true)
        {
            let login = $('#login_surname').val();
            UserDataService.getUser(login, getUserSuccess, getUserError);
        }
        else
        {
            loginErrorHandler(data, 'identifiants incorrects.');
        }
    }
    else
    {
        loginErrorHandler(data, 'la vérification des identifiants a échoué.');
    }
}

function loginError(res)
{
    loginErrorHandler(res, 'vérification des identifiants.');
}

function getUserSuccess(data)
{
    console.log(data);
    //
    if(SessionHelper.connectUser(data))
    {
        window.location.assign(CARD_HOME);
    }
    else
    {
        loginErrorHandler(null, "Erreur lors du renseignement des données récupérées");
    }
}

function getUserError(res)
{
    loginErrorHandler(res, 'récupération du profil impossible.');
}

function loginErrorHandler(res, message)
{
    message = 'Erreur de connexion : ' + message;
    //
    console.log(res);
    console.log(message);
    //
    alert(message);
}

function login()
{
    let login = $('#login_surname').val();
    let password = $('#login_password').val();

    AuthService.checkCredentials(login, password, loginSuccess, loginError);
}

//======================================================================================================================
//======================================================= SCRIPT =======================================================
//======================================================================================================================

console.log("Loaded : sign_in.js");

if(SessionHelper.isUserLogged())
{
    window.location.assign(CARD_HOME);
}
else
{
    $(document).ready(function()
    {
        $('#form-signin').bind('keypress keydown keyup', function(e)
        {
            if(e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13)
            {
                e.preventDefault();
                login();
                return false;
            }
        });

        $('#form-signin').submit(function(e)
        {
            e.preventDefault();
            login();
            return false;
        });

        $("#button-cancel").click(function(e)
        {
            // Réinitialisation champs
            $('#login_surname').val('');
            $('#login_password').val('');
        });
    })
}