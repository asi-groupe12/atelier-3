
//======================================================================================================================
//====================================================== IMPORTS =======================================================
//======================================================================================================================

import { UserDataService }                      from './utils/api-userdata-helper.js'
import { CARD_MARKET, CARD_PROFILE, SIGN_IN }   from './utils/web-pages-helper.js'
import { SessionHelper }                        from './utils/web-session-helper.js'

//======================================================================================================================
//===================================================== FUNCTIONS ======================================================
//======================================================================================================================

function getUserInfoSuccess(data)
{
    SessionHelper.setUserInfo(data);
    //if(SessionHelper.setUserInfo(data))
    //{
        $('#moneyHeader').text(SessionHelper.getUserBalance());
        $('#userHeader').text(SessionHelper.getUsername());
    //}
}

function getUserInfoError(res)
{
    console.log("Couldn't get User Information from server");

    // TODO handle ? display to the user ? make next js/page calls try again ?
}

//======================================================================================================================
//======================================================= SCRIPT =======================================================
//======================================================================================================================

if(SessionHelper.isUserLogged())
{
    $(document).ready(function()
    {
        UserDataService.getUser(SessionHelper.getUserID(), getUserInfoSuccess, getUserInfoError);

        $("#playButtonId").click(function()
        {
            //TODO
        });

        $("#buyButtonId").click(function()
        {
            window.location.assign(CARD_MARKET);
        });

        $("#sellButtonId").click(function()
        {
            window.location.assign(CARD_PROFILE);
        });
    });
}
else
{
    window.location.assign(SIGN_IN);
}