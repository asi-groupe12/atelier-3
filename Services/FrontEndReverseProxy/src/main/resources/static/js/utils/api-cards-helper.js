
import { ApiRequest} from "./api-common-helper.js";

export class CardService
{
    static urlAPI = "/cards";

    static getCards(success, error)
    {
        const type = 'GET';
        const data = null;
        let url = this.urlAPI;

        ApiRequest(url, type, data, success, error);
    }
    
    static getCardByName(param, success, error)
    {
        const type = 'GET';
        const data = null;
        let url = this.urlAPI + '/name/' + param;;

        ApiRequest(url, type, data, success, error);
    }
    
    static getCardsByUserId(param, success, error)
    {
        const type = 'GET';
        const data = null;
        let url = this.urlAPI + '/user/' + param;;

        ApiRequest(url, type, data, success, error);
    }
}