
import { ApiRequest} from "./api-common-helper.js";

export class MarketService
{
    static urlAPI = "/market";

    static buyCard(userId, cardId, success, error)
    {
        const type = 'PUT';
        const data = null;
        let url = this.urlAPI + "/buyCard/user/" + userId + "/card/" + cardId;

        ApiRequest(url, type, data, success, error);
    }
    
    static sellCard(userId, cardId, success, error)
    {
        const type = 'DELETE';
        const data = null;
        let url = this.urlAPI + "/sellCard/user/" + userId + "/card/" + cardId;

        ApiRequest(url, type, data, success, error);
    }
    
}