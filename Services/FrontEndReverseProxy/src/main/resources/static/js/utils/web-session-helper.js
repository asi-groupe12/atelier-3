
import { UserDTO } from "./api-common-helper.js";

export class SessionHelper
{
    static USER = 'user'; // todo remove or use only this with a userDTO inside
    static USER_ID = 'user.id';
    static USER_LOGIN = 'user.login';
    static USER_FNAME = 'user.firstName';
    static USER_LNAME = 'user.lastName';
    static USER_BALANCE = 'user.balance';

    static isUserLogged()
    {
        // this should normally be sufficient to check undefined/null/""/empty/etc
        return sessionStorage.getItem(this.USER) && sessionStorage.getItem(this.USER_ID) && sessionStorage.getItem(this.USER_LOGIN);
    }

    static connectUser(userDTO)
    {
        if((userDTO))
        {
            // Turn it into a UserDTO if needed
            if((userDTO instanceof UserDTO) === false)
            {
                userDTO = UserDTO.identification(userDTO.id, userDTO.login);
            }

            // Fill sessionStorage with gathered data
            sessionStorage.setItem(this.USER, userDTO.id); // todo replace
            sessionStorage.setItem(this.USER_ID, userDTO.id);
            sessionStorage.setItem(this.USER_LOGIN, userDTO.login);
            return true;
        }
        else
        {
            console.log("connectUser failed");
            return false;
        }
    }

    static setUserInfo(userDTO)
    {
        if((userDTO))
        {
            // Turn it into a UserDTO if needed
            if((userDTO instanceof UserDTO) === false)
            {
                userDTO = UserDTO.full(userDTO.id, userDTO.login, "Ce serait trop facile, tu ne crois pas ?", userDTO.firstName, userDTO.lastName, userDTO.balance);
            }

            // Fill sessionStorage with gathered data
            sessionStorage.setItem(this.USER_FNAME, userDTO.firstName);
            sessionStorage.setItem(this.USER_LNAME, userDTO.lastName);
            sessionStorage.setItem(this.USER_BALANCE, userDTO.balance);
            return true;
        }
        else
        {
            console.log("setUserInfo failed");
            return false;
        }
    }

    static disconnectUser()
    {
        sessionStorage.removeItem(this.USER); // todo replace
        sessionStorage.removeItem(this.USER_ID);
        sessionStorage.removeItem(this.USER_LOGIN);

        sessionStorage.removeItem(this.USER_FNAME);
        sessionStorage.removeItem(this.USER_LNAME);
        sessionStorage.removeItem(this.USER_BALANCE);
    }

    static getUserID()
    {
        if(SessionHelper.isUserLogged())
        {
            return sessionStorage.getItem(this.USER_ID);
        }
        else return null;
    }

    static getUsername()
    {
        if(SessionHelper.isUserLogged())
        {
            return sessionStorage.getItem(this.USER_LOGIN);
        }
        else return null;
    }

    static getUserBalance()
    {
        if(SessionHelper.isUserLogged())
        {
            return sessionStorage.getItem(this.USER_BALANCE);
        }
        else return null;
    }

}