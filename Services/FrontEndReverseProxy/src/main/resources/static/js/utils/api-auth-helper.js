
import { ApiRequest } from "./api-common-helper.js";

export class AuthService
{
    static urlAPI = "/auth";

    static checkCredentials(login, password, success, error)
    {
        const url = this.urlAPI + "/login";
        const type = 'POST';
        const data = { 'login': login, 'password' : password };

        ApiRequest(url, type, data, success, error);
    }

    static registerUser(userDTO, success, error)
    {
        const url = this.urlAPI + "/account";
        const type = 'POST';
        const data = userDTO; //todo check  if UserDTO ?

        console.log(data);

        ApiRequest(url, type, data, success, error);
    }
}