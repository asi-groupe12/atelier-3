
import { ApiRequest} from "./api-common-helper.js";

export class UserDataService
{
    static urlAPI = "/users";

    static getUser(param, success, error)
    {
        const type = 'GET';
        const data = null;
        let url = this.urlAPI;

        if(isNaN(param)) //https://stackoverflow.com/questions/175739/built-in-way-in-javascript-to-check-if-a-string-is-a-valid-number
        {
            // Default behavior : getUserByLogin
            url = url + '/login/' + param;
        }
        else
        {
            // Default behavior : getUserByID
            url = url + '/id/' + parseInt(param);
        }

        ApiRequest(url, type, data, success, error);
    }
}