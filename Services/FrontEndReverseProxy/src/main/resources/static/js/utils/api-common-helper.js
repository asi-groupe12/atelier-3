
export function ApiRequest(reqURL, reqType, reqData, success, error)
{
    $.ajax({
        type: reqType,
        url: reqURL,
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
        },
        data: JSON.stringify(reqData),
        success: function(data) { success(data); },
        error: function(res) { error(res); },
    });
}

export class UserDTO
{
    static empty()
    {
        let userDTO = new UserDTO();
        userDTO.login = "";
        userDTO.password = "";
        userDTO.firstName = "";
        userDTO.lastName = "";

        return userDTO;
    }

    static identification(id, login)
    {
        let userDTO = new UserDTO();
        userDTO.id = id;
        userDTO.login = login;

        return userDTO;
    }

    static basic(login, password, firstName, lastName)
    {
        let userDTO = new UserDTO();
        userDTO.login = login;
        userDTO.password = password;
        userDTO.firstName = firstName;
        userDTO.lastName = lastName;

        return userDTO;
    }

    static full(id, login, password, firstName, lastName, balance)
    {
        let userDTO = new UserDTO();
        userDTO.id = id;
        userDTO.login = login;
        userDTO.password = password;
        userDTO.firstName = firstName;
        userDTO.lastName = lastName;
        userDTO.balance = balance;

        return userDTO;
    }

}