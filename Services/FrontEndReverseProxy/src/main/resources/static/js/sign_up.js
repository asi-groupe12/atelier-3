
//======================================================================================================================
//====================================================== IMPORTS =======================================================
//======================================================================================================================

import { AuthService }      from './utils/api-auth-helper.js'
import { UserDTO }          from './utils/api-common-helper.js'
import { CARD_HOME }        from './utils/web-pages-helper.js'
import { SessionHelper }    from './utils/web-session-helper.js'

//======================================================================================================================
//===================================================== FUNCTIONS ======================================================
//======================================================================================================================

function checkPassword()
{
    let password_one = $('#user_password').val().trim();
    let password_two = $('#user_repassword').val().trim();
    const password_error = $("#password-error");

    password_error.text("");

    if(password_one.length > 3)
    {
        if(password_one !== password_two)
        {
            password_error.text("Les mots de passe ne correspondent pas.");
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        password_error.text("Le mot de passe est trop court.");
        return false;
    }
}

function clearAll()
{
    $('#user_name').val("");
    $('#user_surname').val("");
    $('#user_password').val("");
    $('#user_repassword').val("");
}

function registerSuccess(data)
{
    console.log(data);
    //
    if(SessionHelper.connectUser(data))
    {
        window.location.assign(CARD_HOME);
    }
    else
    {
        registerErrorHandler(null, "Erreur lors du renseignement des données récupérées");
    }
}

function registerError(res)
{
    registerErrorHandler(res, "impossible de réaliser l'inscription.");
}

function registerErrorHandler(res, message)
{
    message = "Erreur de l'inscription : " + message;
    //
    console.log(res);
    console.log(message);
    //
    alert(message);
}

function register()
{
    if(checkPassword())
    {
        let login = $('#user_login').val().trim();
        let password = $('#user_password').val().trim();
        let firstName = $('#user_fname').val().trim();
        let lastName = $('#user_lname').val().trim();
        //
        let userDTO = UserDTO.basic(login, password, firstName, lastName);

        AuthService.registerUser(userDTO, registerSuccess, registerError);
    }
}

//======================================================================================================================
//======================================================= SCRIPT =======================================================
//======================================================================================================================

console.log("Loaded : sign_up.js");

if(SessionHelper.isUserLogged())
{
    window.location.assign(CARD_HOME);
}
else
{
    $(document).ready(function()
    {
        $('#user_repassword').submit(function()
        {
            checkPassword();
        });

        $('#form-signup').bind('keypress keydown keyup', function(e)
        {
            if(e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13)
            {
                event.preventDefault();
                register();
                return false;
            }
        });

        $('#form-signup').submit(function(e)
        {
            e.preventDefault();
            register();
            return false;
        });

        $("#button-cancel").click(function(e)
        {
            clearAll();
        });
    })
}