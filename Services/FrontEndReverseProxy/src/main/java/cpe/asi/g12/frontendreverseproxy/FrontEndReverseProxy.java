package cpe.asi.g12.frontendreverseproxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class FrontEndReverseProxy
{
    public static void main(String[] args)
    {
        SpringApplication.run(FrontEndReverseProxy.class, args);
    }
}