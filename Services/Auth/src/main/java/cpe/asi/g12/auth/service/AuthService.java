package cpe.asi.g12.auth.service;

import cpe.asi.g12.common.api.UserDataServiceProxy;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService
{
    private final UserDataServiceProxy userDataServiceProxy;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public AuthService(UserDataServiceProxy userDataServiceProxy) { this.userDataServiceProxy = userDataServiceProxy; }

    public String getProxiesNames() { return userDataServiceProxy.getName(); }

    /**
     * Verifies the given password for the given login, calling UserData service/proxy
     *
     * @param login
     * @param password
     * @return
     */
    public boolean login(String login, String password)
    {
        ResponseEntity<UserDTO> response = userDataServiceProxy.getUserByLogin(login);

        if(response.getStatusCode() == HttpStatus.OK && response.getBody() != null)
        {
            UserDTO user = response.getBody();

            if(user.getPassword().equals(hash(password)))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Modify User's password, calling UserData service/proxy
     *
     * @param id
     * @param password
     * @return
     */
    public boolean changePassword(int id, String password)
    {
        ResponseEntity<UserDTO> userResponse = userDataServiceProxy.getUserByID(id);

        if(userResponse.getStatusCode() == HttpStatus.OK && userResponse.getBody() != null)
        {
            UserDTO user = userResponse.getBody();
            user.setPassword(hash(password));

            ResponseEntity<UpdateResponseDTO<UserDTO>> updateResponse = userDataServiceProxy.updateUser(user.getID(), user);

            if(updateResponse.getStatusCode() == HttpStatus.OK && updateResponse.getBody() != null)
            {
            	//Verification du bon changement de mot de passe
                UpdateResponseDTO<UserDTO> users = updateResponse.getBody();
                if((users.getNewItem().getPassword()).equals(user.getPassword())) {
                	return true;
                }
                
            }
        }
        return false;
    }

    /**
     * Create an User, calling UserData service/proxy
     *
     * @param userDTO
     * @return
     */
    public Optional<UserDTO> register(UserDTO userDTO)
    {
        // TODO check input (login not empty, login not exist, ...)
        System.out.println(userDTO);

        userDTO.setPassword(hash(userDTO.getPassword()));
	    ResponseEntity<UserDTO> registerResponse = userDataServiceProxy.addUser(userDTO);

        if(registerResponse.getStatusCode() == HttpStatus.OK && registerResponse.getBody() != null)
        {
            return Optional.of(registerResponse.getBody());
        }

        return Optional.empty();
    }

    /**
     * Remove a User, calling UserData service/proxy
     *
     * @param id
     * @return
     */
    public boolean unregister(int id)
    {
        ResponseEntity<Boolean> registerResponse = userDataServiceProxy.removeUser(id);

        if(registerResponse.getStatusCode() == HttpStatus.OK && registerResponse.getBody() != null)
        {
            return registerResponse.getBody();
        }

        return false;
    }
	
	/**
	 * Hash a String with SHA-256 algorithm
	 * 
	 * @param password
	 * @return password hashed
	 */
	private String hash(String password) {
		return DigestUtils.sha256Hex(password);
	}

}
