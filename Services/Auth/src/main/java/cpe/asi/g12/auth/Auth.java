package cpe.asi.g12.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"cpe.asi.g12.common"})
public class Auth
{
    public static void main(String[] args)
    {
        SpringApplication.run(Auth.class,args);
    }
}