package cpe.asi.g12.auth.controller;

import cpe.asi.g12.common.api.AuthServiceProxy;
import cpe.asi.g12.common.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import cpe.asi.g12.auth.service.AuthService;

import java.util.Optional;

@RestController
public class AuthRest implements AuthServiceProxy
{
	private final AuthService authService;

    @Autowired
    public AuthRest(AuthService authService) {
        this.authService = authService;
    }

    public String getName() { return "auth"; }

    public String getNames() { return getName() + System.lineSeparator() + authService.getProxiesNames(); }

    @Override
    public ResponseEntity<Boolean> login(UserDTO userDTO) { return ResponseEntity.of(Optional.of(authService.login(userDTO.getLogin(), userDTO.getPassword()))); }

    @Override
    public ResponseEntity<Boolean> changePassword(UserDTO userDTO) { return ResponseEntity.of(Optional.of(authService.changePassword(userDTO.getID(), userDTO.getPassword()))); }

    @Override
    public ResponseEntity<UserDTO> register(UserDTO userDTO) { return ResponseEntity.of(authService.register(userDTO)); }

    @Override
    public ResponseEntity<Boolean> unregister(int id) { return ResponseEntity.of(Optional.of(authService.unregister(id))); }
}
