package cpe.asi.g12.auth.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import cpe.asi.g12.common.api.UserDataServiceProxy;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthService.class)
public class AuthServiceTest {

	@Autowired
	private AuthService authService;
	
	@MockBean
	private UserDataServiceProxy userDataServiceProxy;
	
	UserDTO defaultUser=new UserDTO();
	
	@Test
	public void loginOK() {
		
		UserDTO successUser = new UserDTO();
		successUser.setLogin("test");
		successUser.setPassword(DigestUtils.sha256Hex("success"));
		
		Mockito.when(
				userDataServiceProxy.getUserByLogin(Mockito.anyString())
				).thenReturn(ResponseEntity.ok(successUser));
		
		boolean result=authService.login("test", "success");
		assertTrue(result);
	}
	
	@Test
	public void loginWrong() {
		
		UserDTO errorUser = new UserDTO();
		errorUser.setLogin("test");
		errorUser.setPassword(DigestUtils.sha256Hex("error"));
		
		Mockito.when(
				userDataServiceProxy.getUserByLogin(Mockito.anyString())
				).thenReturn(ResponseEntity.ok(errorUser));
		
		boolean result=authService.login("test", "eror");
		assertFalse(result);
	}
	
	@Test
	public void changePassword() {
		
		UserDTO oldUser = new UserDTO();
		oldUser.setID(1);
		
		UserDTO updatedUser = new UserDTO();
		updatedUser.setID(1);
		updatedUser.setPassword(DigestUtils.sha256Hex("test"));
		
		UpdateResponseDTO<UserDTO> updatedReponse = new UpdateResponseDTO<UserDTO>(oldUser, updatedUser);
		
		Mockito.when(
				userDataServiceProxy.getUserByID(Mockito.anyInt())
				).thenReturn(ResponseEntity.ok(oldUser));
		
		Mockito.when(
				userDataServiceProxy.updateUser(Mockito.anyInt(), Mockito.any(UserDTO.class))
				).thenReturn(ResponseEntity.ok(updatedReponse));
		
		boolean result=authService.changePassword(1, "test");
		assertTrue(result);
	}

}
