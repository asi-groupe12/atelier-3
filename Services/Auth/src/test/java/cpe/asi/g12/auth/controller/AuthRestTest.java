package cpe.asi.g12.auth.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cpe.asi.g12.auth.service.AuthService;
import cpe.asi.g12.common.dto.UserDTO;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthRest.class)
public class AuthRestTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AuthService authService;
	
	String mockName = "auth";
	
	UserDTO mockUser = new UserDTO();
	
	private final String ENCODING = "utf-8";
	
	@Test
	public void getName() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth//name").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertTrue(mockName.equals(result.getResponse()
				.getContentAsString()));
	}

	
	@Test
	public void registerUser() throws Exception {
		
		UserDTO userToReturn = new UserDTO();
		
		userToReturn.setID(1);
		userToReturn.setLogin("success");
		
		Mockito.when(
				authService.register(Mockito.any(UserDTO.class))
				).thenReturn(Optional.ofNullable(userToReturn));
		
		String expectedJson="{\"id\":-1,\"login\":\"\",\"password\":\"\",\"firstName\":\"\",\"lastName\":\"\",\"balance\":0}";
	    
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/account").accept(MediaType.APPLICATION_JSON_UTF8).contentType(MediaType.APPLICATION_JSON_UTF8).content(expectedJson).characterEncoding(ENCODING);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		int status = result.getResponse().getStatus();
		assertEquals(200, status);
	}
	
}
