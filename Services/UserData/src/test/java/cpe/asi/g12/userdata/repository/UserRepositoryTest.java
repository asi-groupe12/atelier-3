package cpe.asi.g12.userdata.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import cpe.asi.g12.userdata.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
    UserRepository userRepository;

    @Before
    public void setUp() {
    	userRepository.save(new User(1,"login", "password", "FirstName", "LastName", 5000));
    }

    @After
    public void cleanUp() {
    	userRepository.deleteAll();
    }
    
    @Test
    public void saveUser() {
    	userRepository.save(new User(1,"login", "password", "FirstName", "LastName", 5000));
        assertTrue(true);
    }
    
    @Test
    public void saveAndGetHero() {
    	userRepository.deleteAll();
    	userRepository.save(new User(2,"login2", "password2", "FirstName2", "LastName2", 5000));
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        assertTrue(userList.size() == 1);
        assertTrue(userList.get(0).getLogin().equals("login2"));
        assertTrue(userList.get(0).getFirstName().equals("FirstName2"));
        assertTrue(userList.get(0).getLastName().equals("LastName2"));
    }
    
    @Test
    public void getUser() {
    	User userSaved = userRepository.save(new User(3,"login3", "password3", "FirstName3", "LastName3", 5000));
        Optional<User> user = userRepository.findById(userSaved.getID());
        assertTrue(user.isPresent());
        assertTrue(user.get().getFirstName().equals("FirstName3"));
        assertTrue(user.get().getLastName().equals("LastName3"));
    }
    
    @Test
    public void findByLogin() {
    	userRepository.save(new User(1,"test1", "password1", "FirstName1", "LastName1", 5000));
    	userRepository.save(new User(2,"test2", "password2", "FirstName2", "LastName2", 5000));
    	userRepository.save(new User(3,"test3", "password3", "FirstName3", "LastName3", 5000));
    	userRepository.save(new User(4,"test4", "password4", "FirstName4", "LastName4", 5000));
        Optional<User> user = null;
        user = userRepository.findByLogin("test1");
        assertTrue(user.isPresent());
    }
	
}
