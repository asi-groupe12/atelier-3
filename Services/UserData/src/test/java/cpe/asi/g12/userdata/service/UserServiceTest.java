package cpe.asi.g12.userdata.service;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;
import cpe.asi.g12.userdata.model.User;
import cpe.asi.g12.userdata.repository.UserRepository;


@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@MockBean
	private UserRepository userRepository;
	
	User tmpUser=new User(1,"login", "password", "FirstName", "LastName", 5000);
	UserDTO tmpUserDTO = tmpUser.toDTO();
	
	@Test
	public void getUser() {
		Mockito.when(
				userRepository.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		Optional<UserDTO> userInfo=userService.getUser(45);
		assertTrue(userInfo.get().toString().equals(tmpUserDTO.toString()));
	}
	
	@Test
	public void addUser() {
		Mockito.when(
				userRepository.save(Mockito.any(User.class))
				).thenReturn(tmpUser);
		Mockito.when(
				userService.getUser(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(tmpUser.toDTO()));
		Mockito.when(
				userRepository.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		
		Optional<UserDTO> userInfo=userService.addUser(tmpUserDTO);
		assertTrue(userInfo.get().toString().equals(tmpUserDTO.toString()));
	}
	
	@Test
	public void updateUser() {
		UserDTO userDTO = tmpUser.toDTO();
		userDTO.setFirstName("Test");
		
		Mockito.when(
				userRepository.save(Mockito.any(User.class))
				).thenReturn(new User(userDTO));
		Mockito.when(
				userService.getUser(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(tmpUser.toDTO()));
		Mockito.when(
				userRepository.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		
		Optional<UpdateResponseDTO<UserDTO>> userInfo=userService.updateUser(userDTO.getID(), userDTO);
		assertTrue(userInfo.get().getNewItem().toString().equals(userDTO.toString()));
	}
	
	@Test
	public void removeUser() {
		Mockito.when(
				userService.getUser(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(tmpUser.toDTO()));
		Mockito.when(
				userRepository.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		
		assertTrue(userService.removeUser(45));
	}
	
}
