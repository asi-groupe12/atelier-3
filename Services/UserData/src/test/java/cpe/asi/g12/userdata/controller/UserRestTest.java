package cpe.asi.g12.userdata.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cpe.asi.g12.common.dto.UserDTO;
import cpe.asi.g12.userdata.model.User;
import cpe.asi.g12.userdata.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRest.class)
public class UserRestTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService userService;
	
	String mockName = "userdata";
	
	User tmpUser=new User(1,"login", "password", "FirstName", "LastName", 5000);
	UserDTO tmpUserDTO = tmpUser.toDTO();
	
	@Test
	public void getName() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users//name").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertTrue(mockName.equals(result.getResponse()
				.getContentAsString()));
	}
	
	@Test
	public void getUser() throws Exception {
		
		Mockito.when(
				userService.getUser(Mockito.anyInt())
				).thenReturn(Optional.ofNullable(tmpUserDTO));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/45").accept(MediaType.APPLICATION_JSON_UTF8).contentType(MediaType.APPLICATION_JSON_UTF8);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(tmpUserDTO);

		int status = result.getResponse().getStatus();
		assertEquals(200, status);
	}
	
}
