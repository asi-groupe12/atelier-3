package cpe.asi.g12.userdata.model;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
	private List<String> stringList;
	private List<Integer> intList;

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add user to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
	}
	
	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN user list");
		stringList = null;
		intList = null;
	}
	
	@Test
	public void createUser() {
		for(String msg:stringList) {
			for(String msg2:stringList) {
				for(String msg3:stringList) {
					for(Integer msg4:intList) {
						User u=new User(msg4, msg, msg2, msg, msg3, msg4);
						System.out.println("msg:"+msg+", msg2:"+msg2+"msg:"+msg+", msg3:"+msg3+", msg4:"+msg4);
						assertTrue(u.getID() == msg4.intValue());
						assertTrue(u.getLogin() == msg);
						assertTrue(u.getPassword() == msg2);
						assertTrue(u.getFirstName() == msg);
						assertTrue(u.getLastName() == msg3);
						assertTrue(u.getBalance() == msg4.intValue());
					}	
				}	
			}
		}
	}
	
	@Test
	public void displayUser() {
		User u=new User(1,"login", "password", "FirstName", "LastName", 5000);
		String expectedResult="User{" +
				"id=" + "1" +
				", login='" + "login" + '\'' +
				", password='" + "password" + '\'' +
				", firstName='" + "FirstName" + '\'' +
				", lastName='" + "LastName" + '\'' +
				", balance=" + "5000" +
				'}';
		assertTrue(u.toString().equals(expectedResult));
		
	}
	
}
