package cpe.asi.g12.userdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserData
{
    public static void main(String[] args)
    {
        SpringApplication.run(UserData.class,args);
    }
}