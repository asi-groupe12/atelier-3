package cpe.asi.g12.userdata.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cpe.asi.g12.common.api.UserDataServiceProxy;
import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;
import cpe.asi.g12.userdata.service.UserService;

@RestController
public class UserRest implements UserDataServiceProxy
{
    private final UserService userService;

    @Autowired
    public UserRest(UserService userService)
    {
        this.userService = userService;
    }

    @Override
    public String getName()
    {
        return "userdata";
    }

    @Override
    public String getNames() { return getName(); }

    @Override
    public List<UserDTO> getUsers() { return userService.getUsers(); }

    @Override
    public ResponseEntity<UserDTO> getUser(int id) { return ResponseEntity.of(userService.getUser(id)); }

    @Override
    public ResponseEntity<UserDTO> getUserByID(int id) { return ResponseEntity.of(userService.getUser(id)); }

    @Override
    public ResponseEntity<UserDTO> getUserByLogin(String login) { return ResponseEntity.of(userService.getUser(login)); }

    @Override
    public ResponseEntity<UserDTO> addUser(UserDTO userDTO) { return ResponseEntity.of(userService.addUser(userDTO)); }

    @Override
    public ResponseEntity<UpdateResponseDTO<UserDTO>> updateUser(int id, UserDTO userDTO){ 
        
    	ResponseEntity<UpdateResponseDTO<UserDTO>> response = ResponseEntity.of(userService.updateUser(id, userDTO));
    	UpdateResponseDTO<UserDTO> users = response.getBody();
    	System.out.println(users.getOriginalItem());
        System.out.println(users.getNewItem());
    	System.out.println(response);
    	
    	return response; 
	}

    @Override
    public ResponseEntity<Boolean> removeUser(int id) { return ResponseEntity.of(Optional.of(userService.removeUser(id))); }

    @Override
    public List<UserDTO> generateUsers() { return userService.generateUsers(); }
}