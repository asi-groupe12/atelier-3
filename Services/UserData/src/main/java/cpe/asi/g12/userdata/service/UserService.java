package cpe.asi.g12.userdata.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cpe.asi.g12.common.dto.UpdateResponseDTO;
import cpe.asi.g12.common.dto.UserDTO;
import cpe.asi.g12.userdata.model.User;
import cpe.asi.g12.userdata.repository.UserRepository;
import cpe.asi.g12.userdata.utils.Constants;

@Service
public class UserService
{
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> getUsers() {
        Iterable<User> usersIt = userRepository.findAll();
        List<UserDTO> usersList = new ArrayList<>();
        usersIt.forEach(user -> usersList.add(user.toDTO()));

        return usersList;
    }

    public Optional<UserDTO> getUser(Integer id)
    {
        Optional<User> user = userRepository.findById(id);

        if (user.isPresent()) { return Optional.of(user.get().toDTO()); }
        else { return Optional.empty(); }
    }

    public Optional<UserDTO> getUser(String login)
    {
        Optional<User> user = userRepository.findByLogin(login);

        if (user.isPresent()) { return Optional.of(user.get().toDTO()); }
        else { return Optional.empty(); }
    }

    public Optional<UserDTO> addUser(UserDTO userDTO)
    {
        userDTO.setBalance(Constants.DEFAULT_MONEY);

        User insertedUser = userRepository.save(new User(userDTO));
        System.out.println(insertedUser.getID());
        return getUser(insertedUser.getID());
    }

    public Optional<UpdateResponseDTO<UserDTO>> updateUser(int id, UserDTO sourceUserDTO)
    {
        Optional<UserDTO> userToUpdate = this.getUser(id);
        if(userToUpdate.isPresent())
        {
        	UserDTO originalUserDTO = userToUpdate.get();
            UserDTO updatedUserDTO = new UserDTO();
            updatedUserDTO.setBalance(originalUserDTO.getBalance());
            updatedUserDTO.setFirstName(originalUserDTO.getFirstName());
            updatedUserDTO.setID(originalUserDTO.getID());
            updatedUserDTO.setLastName(originalUserDTO.getLastName());
            updatedUserDTO.setLogin(originalUserDTO.getLogin());
            updatedUserDTO.setPassword(originalUserDTO.getPassword());
            updatedUserDTO = this.updateUserWithSourceUser(updatedUserDTO, sourceUserDTO);

            User updatedUser = userRepository.save(new User(updatedUserDTO));
            System.out.println("\nORIGINAL => "+originalUserDTO);
            System.out.println("\nUPDATED => "+updatedUser.toDTO());
            UpdateResponseDTO<UserDTO> updateResponse = new UpdateResponseDTO<UserDTO>(originalUserDTO,updatedUser.toDTO());
            System.out.println("\nUPDATE RESPONSE => "+updateResponse);
            return Optional.of(updateResponse);
        }
        else { return Optional.empty(); }
    }

    private UserDTO updateUserWithSourceUser(UserDTO userToUpdate, UserDTO sourceUser)
    {
        if(sourceUser.getLogin() != null && sourceUser.getLogin().isEmpty() == false)
        {
            userToUpdate.setLogin(sourceUser.getLogin());
        }
        if(sourceUser.getPassword() != null && sourceUser.getPassword().isEmpty() == false)
        {
            userToUpdate.setPassword(sourceUser.getPassword());
        }
        if(sourceUser.getFirstName() != null && sourceUser.getFirstName().isEmpty() == false)
        {
            userToUpdate.setFirstName(sourceUser.getFirstName());
        }
        if(sourceUser.getLastName() != null && sourceUser.getLastName().isEmpty() == false)
        {
            userToUpdate.setLastName(sourceUser.getLastName());
        }
        if(sourceUser.getBalance() != -1)
        {
            userToUpdate.setBalance(sourceUser.getBalance());
        }

        return userToUpdate;
    }

    public boolean removeUser(int id)
    {
        Optional<UserDTO> userToRemoveDTO = this.getUser(id);
        if(userToRemoveDTO.isPresent())
        {
            userRepository.delete(new User(userToRemoveDTO.get()));
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<UserDTO> generateUsers()
    {
        List<UserDTO> userDTOs = new ArrayList<>();

        for(int i = 0; i < 10; i++)
        {
            UserDTO userDTO = new UserDTO();
            userDTO.setLogin(UUID.randomUUID().toString());
            userDTO.setPassword("password");
            userDTO.setFirstName("Jean-Random");
            userDTO.setLastName("Randomize");

            Optional<UserDTO> insertedUserDTO = addUser(userDTO);
            insertedUserDTO.ifPresent(userDTOs::add);
        }

        return userDTOs;
    }

}