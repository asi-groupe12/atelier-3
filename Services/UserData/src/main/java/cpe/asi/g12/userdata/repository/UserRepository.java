package cpe.asi.g12.userdata.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cpe.asi.g12.userdata.model.User;

public interface UserRepository extends CrudRepository<User, Integer>
{

    public Optional<User> findByLogin(String login);

}

