package cpe.asi.g12.userdata.model;

import cpe.asi.g12.common.dto.UserDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "app_user")
public class User
{

	//region User - Attributes
	
	@Id
	@GeneratedValue
	private Integer id;
	//
	@Column(unique=true)
	private String login;
	private String password;
	//
	private String firstName;
	private String lastName;
	//
	private int balance;

	//endregion

	//region User - Constructors

	/**
	 * User - Empty Constructor
	 */
	public User()
	{
		this(-1, "", "", "", "", 0);
	}

	/**
	 * User - Full Constructor
	 */
	public User(int id, String login, String password, String firstName, String lastName, int balance)
	{
		this.id = id;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.balance = balance;
	}

	/**
	 * User - Full Constructor without ID
	 */
	public User(String login, String password, String firstName, String lastName, int balance)
	{
		this(-1, login, password, firstName, lastName, balance);
	}

	/**
	 * User - Constructor by DTO
	 */
	public User(UserDTO userDTO)
	{
		this.id = userDTO.getID();
        this.login = userDTO.getLogin();
        this.password = userDTO.getPassword();
        this.firstName = userDTO.getFirstName();
        this.lastName = userDTO.getLastName();
        this.balance = userDTO.getBalance();
	}

	//endregion

    public UserDTO toDTO()
    {
        UserDTO userDTO = new UserDTO();

        userDTO.setID(this.id);
        userDTO.setLogin(this.login);
        userDTO.setPassword(this.password);
        userDTO.setFirstName(this.firstName);
        userDTO.setLastName(this.lastName);
        userDTO.setBalance(this.balance);

        return userDTO;
    }

	//region User - Getters & Setters

	public Integer getID() { return id; }
	//public void setID(Integer id) { this.id = id; }

	public String getLogin() { return login; }
	public void setLogin(String login) { this.login = login; }

	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }

	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }

	public String getLastName() { return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }

	public int getBalance() { return balance; }
	public void setBalance(int balance) { this.balance = balance; }

	//endregion

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", balance=" + balance +
				'}';
	}
}
