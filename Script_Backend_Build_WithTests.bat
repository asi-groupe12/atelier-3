

:: Build 'Common' package
call mvn -f Common/pom.xml clean install

:: Build 'Services' packages
call mvn -f Services/Auth/pom.xml clean install
call mvn -f Services/CardsManagement/pom.xml clean install
call mvn -f Services/Market/pom.xml clean install
call mvn -f Services/UserData/pom.xml clean install

@echo "Backend built and tested !"
pause