# ASI - Atelier 3

## Membres du groupe

 - CINQUIN Benjamin
 - CLEMENTEÏ Lucas
 - GARCIA Tom-Brian
 - MARREL Lucas

## Eléments réalisés du cahier des charges PAR MEMBRE du groupe.

 - **CINQUIN Benjamin**

    - Développement Service Auth
    - Développement des tests sur UserData
    - Développement des tests sur Auth
    - Développement Front-end
    - Intégration Continue
 
 - **CLEMENTEÏ Lucas**

    - Développement Service UserData
    - Développement Service Market
    - Développement des tests sur Market
    - Développement Front-end
    - Développement du package Common
 
 - **GARCIA Tom-Brian**

    - Développement Front-end
    - Mise en place de ZUUL Reverse-Proxy
    - Intégration de Feign
    - Intégration de Swagger
    - Développement du package Common 
    - Commencement Service Game
    - Commencement Service Room
    - Intégration Continue
 
 - **MARREL Lucas**

    - Développement Service Cards
    - Développement des tests sur Cards

## Eléments non-réalisés du cahier des charges

- Service Game
- Service Room

## Eléments réalisés en plus du cahier des charges

- Mise en place de ZUUL Reverse-Proxy
- Intégration de Feign
- Intégration de Swagger