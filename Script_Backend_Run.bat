
:: Run 'Services' packages, simultaneously

start "UserData Service" java -jar Services\UserData\target\UserData-1.0-SNAPSHOT.jar
start "Auth Service" java -jar Services\Auth\target\Auth-1.0-SNAPSHOT.jar
start "CardsManagement Service" java -jar Services\CardsManagement\target\CardsManagement-1.0-SNAPSHOT.jar
start "Market Service" java -jar Services\Market\target\Market-1.0-SNAPSHOT.jar

pause